package unanet.translator.functions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestVariables {

	@Before
	public void setUp() {
	    //This is done to get code coverage for the header of the class.
        //Annoying.
	    new Variables();
	    Variables.NEWVAR("Test");
	}

	@After
    public void cleanUp() {
	    Variables.vars = new HashMap<>();
    }

	@Test
    public void TestSETVAR() {
        Variables.SETVAR("Test", "Value");
        assertEquals( "Value", Variables.vars.get("Test"));

        Variables.SETVAR("Test", "Value2");
        assertEquals( "Value2", Variables.vars.get("Test"));

        Variables.SETVAR("Test", null);
        assertNull(Variables.vars.get("Test"));
    }

    @Test
    public void TestGETVAR() {
        assertEquals("", Variables.GETVAR("Test"));

        Variables.vars.put("Test", "Value");
        assertEquals("Value", Variables.GETVAR("Test"));

        Variables.vars.put("Test", "");
        assertEquals("", Variables.GETVAR("Test"));
	}
}
