package unanet.translator.functions;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Translator;
import unanet.translator.exceptions.FunctionResultException;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class TestStandardLibrary {

    @Before
    public void setUp() {
        //This is done to get code coverage for the header of the class.
        //Annoying.
        Translator.Reset();
        Translator.SetCurrentRow(new String[]{"Apples", "Bananas"});
    }
    
    @Test
    public void TestC() {
        Translator.SetCurrentRow(new String[] {"A", "B", "C"});
        assertEquals(StandardLibrary.C("1"), "A");
        assertEquals(StandardLibrary.C("2"), "B");
        assertEquals(StandardLibrary.C("3"), "C");
    }

    @Test(expected = FunctionResultException.class)
    public void TestCFails1() {
        Translator.SetCurrentRow(new String[] {"A", "B", "C"});
        StandardLibrary.C("A");
        StandardLibrary.C("-1");
        StandardLibrary.C("4");
    }

    @Test(expected = FunctionResultException.class)
    public void TestCFails2() {
        Translator.SetCurrentRow(new String[] {"A", "B", "C"});
        StandardLibrary.C("-1");
    }

    @Test(expected = FunctionResultException.class)
    public void TestCFails3() {
        Translator.SetCurrentRow(new String[] {"A", "B", "C"});
        StandardLibrary.C("4");
    }

    @Test(expected = FunctionResultException.class)
    public void TestCFails4() {
        Translator.SetCurrentRow(null);
        StandardLibrary.C("1");
    }

    @Test(expected = FunctionResultException.class)
    public void TestCFails5() {
        Translator.SetCurrentRow(new String[]{"A"});
        StandardLibrary.C("-1");
    }

    @Test
    public void TestUPPER() {
        assertEquals("HELLO", StandardLibrary.UPPER("heLlo"));
    }
    
    @Test
    public void TestLOWER() {
        assertEquals("hello", StandardLibrary.LOWER("HElLO"));
    }
    
    @Test
    public void TestBLANK() {
        assertEquals("", StandardLibrary.BLANK());
    }
    
    @Test
    public void TestFC() {
        Translator.SetCurrentRow(new String[] {"ABC","ABC","ABC"});
        assertEquals("ABCA", StandardLibrary.FC("0", "4"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestFCFails1() {
        StandardLibrary.FC("A", "4");
    }

    @Test(expected = FunctionResultException.class)
    public void TestFCFails2() {
        StandardLibrary.FC("-1", "4");
    }

    @Test(expected = FunctionResultException.class)
    public void TestFCFails3() {
        StandardLibrary.FC("2", "3000");
    }

    @Test(expected = FunctionResultException.class)
    public void TestFCFails4() {
        StandardLibrary.FC("0", "A");
    }

    @Test
    public void TestIGNORE() {
        assertFalse(Translator.CheckIgnore());
        StandardLibrary.IGNORE();
        assertTrue(Translator.CheckIgnore());
    }
    
    @Test
    public void TestLEFT() {
        assertEquals("HelloWo", StandardLibrary.LEFT("7", "HelloWorld"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestLEFTFail() {
        StandardLibrary.LEFT("A", "HelloWorld");
    }
    
    @Test
    public void TestRIGHT() {
        assertEquals("orld", StandardLibrary.RIGHT("4", "HelloWorld"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestRIGHTFail() {
        StandardLibrary.RIGHT("A", "HelloWorld");
    }

    @Test
    public void TestSEQ() {
        assertEquals("0", StandardLibrary.SEQ("0"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestSEQFail() {
        StandardLibrary.SEQ("A");
    }
    
    @Test
    public void TestLENGTH() {
        assertEquals(0, StandardLibrary.LENGTH(""));
        assertEquals(4, StandardLibrary.LENGTH("ABCD"));
    }
    
    @Test
    public void TestMATH() {
        assertEquals("15",   StandardLibrary.MATH("10 + 5", "0"));
        assertEquals("5",    StandardLibrary.MATH("15/3", "0"));
        assertEquals("0.00", StandardLibrary.MATH("sin(pi)", "2"));
        assertEquals("1.41", StandardLibrary.MATH("sqrt(2)", "2"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestMATHFail() {
        StandardLibrary.MATH("2+2", "a1");
    }

    @Test
    public void TestREPLACE() {
        assertEquals("HHelloWorldlloWorld", StandardLibrary.REPLACE("HelloWorld", "e", "HelloWorld"));
    }

    @Test
    public void TestTRIM() {
        String testStr = "   Hello, world!    \n\n";
        assertEquals("Hello, world!", StandardLibrary.TRIM(testStr));
        assertEquals("Hello, world!    \n\n", StandardLibrary.LTRIM(testStr));
        assertEquals("   Hello, world!", StandardLibrary.RTRIM(testStr));

        testStr = "Hello, world!";
        assertEquals("Hello, world!", StandardLibrary.TRIM(testStr));
        assertEquals("Hello, world!", StandardLibrary.LTRIM(testStr));
        assertEquals("Hello, world!", StandardLibrary.RTRIM(testStr));

        testStr = "";
        assertEquals("", StandardLibrary.TRIM(testStr));
        assertEquals("", StandardLibrary.LTRIM(testStr));
        assertEquals("", StandardLibrary.RTRIM(testStr));
    }

    @Test
    public void TestSEARCH() {
        String testStr = "Hello, world!";
        assertEquals(4, StandardLibrary.SEARCH(testStr, "o"));
        assertEquals(12, StandardLibrary.SEARCH(testStr, "!"));
        assertEquals(-1, StandardLibrary.SEARCH(testStr, "-"));
    }

    @Test
    public void TestTODAY() {
        String dateFormat = "dd-MM-yyyy";

        Date curDat = new Date();
        SimpleDateFormat form = new SimpleDateFormat(dateFormat);

        assertEquals(form.format(curDat), StandardLibrary.TODAY(dateFormat));
    }

    @Test
    public void TestEQUAL() {
        assertTrue(StandardLibrary.EQUAL("A", "A"));
        assertFalse(StandardLibrary.EQUAL("A", "B"));

        assertTrue(StandardLibrary.EQUAL("1.00", "1"));
        assertFalse(StandardLibrary.EQUAL("1.00", "1.02"));

        assertFalse(StandardLibrary.EQUAL("1.00", "1.02."));
        assertFalse(StandardLibrary.EQUAL("1.0.0", "1.0."));
    }

    @Test
    public void TestAND() {
        assertTrue(StandardLibrary.AND("true", "true"));
        assertFalse(StandardLibrary.AND("true", "false"));
    }

    @Test
    public void TestOR() {
        assertTrue(StandardLibrary.OR("true", "false"));
        assertTrue(StandardLibrary.OR("false", "true"));
        assertFalse(StandardLibrary.OR("false", "false"));
    }

    @Test
    public void TestNEQUAL() {
        assertTrue(StandardLibrary.NEQUAL("true", "false"));
        assertFalse(StandardLibrary.NEQUAL("true", "true"));
        assertFalse(StandardLibrary.NEQUAL("false", "false"));
    }

    @Test
    public void TestNOT() {
        assertTrue(StandardLibrary.NOT("false"));
        assertFalse(StandardLibrary.NOT("true"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestNOTFail() {
        StandardLibrary.NOT("asda");
    }

    @Test
    public void TestLESS() {
        assertTrue(StandardLibrary.LESS("1.0", "2"));
        assertFalse(StandardLibrary.LESS("3", "2.2"));

        assertTrue(StandardLibrary.LESS("A", "B"));
        assertFalse(StandardLibrary.LESS("B", "A"));
    }

    @Test
    public void TestLESSE() {
        assertTrue(StandardLibrary.LESSE("1.0", "2"));
        assertFalse(StandardLibrary.LESSE("3", "2.2"));
        assertTrue(StandardLibrary.LESSE("3", "3"));

        assertTrue(StandardLibrary.LESSE("A", "B"));
        assertFalse(StandardLibrary.LESSE("B", "A"));
        assertTrue(StandardLibrary.LESSE("A", "A"));
    }

    @Test
    public void TestGREATERE() {
        assertTrue(StandardLibrary.GREATERE("3.0", "2"));
        assertFalse(StandardLibrary.GREATERE("1", "2.2"));
        assertTrue(StandardLibrary.GREATERE("3", "3"));

        assertTrue(StandardLibrary.GREATERE("B", "A"));
        assertFalse(StandardLibrary.GREATERE("A", "B"));
        assertTrue(StandardLibrary.GREATERE("B", "B"));
    }

    @Test
    public void TestGREATER() {
        assertTrue(StandardLibrary.GREATER("16", "2.7"));
        assertFalse(StandardLibrary.GREATER("1", "2.2"));

        assertTrue(StandardLibrary.GREATER("B", "A"));
        assertFalse(StandardLibrary.GREATER("A", "B"));
    }

    @Test
    public void TestNULL() {
        assertNull(StandardLibrary.NULL());
    }

    @Test
    public void TestISNUMERIC() {
        assertTrue(StandardLibrary.ISNUMERIC("15"));
        assertTrue(StandardLibrary.ISNUMERIC("15.2"));
        assertTrue(StandardLibrary.ISNUMERIC("15.7"));
        assertTrue(StandardLibrary.ISNUMERIC("-15.7"));

        assertFalse(StandardLibrary.ISNUMERIC("15.7.2"));
        assertFalse(StandardLibrary.ISNUMERIC("15a"));
        assertFalse(StandardLibrary.ISNUMERIC("appples1231"));
    }

    @Test
    public void TestSUBSTR() {
        assertEquals("pple", StandardLibrary.SUBSTR("apples", "1", "5"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFail1() {
        StandardLibrary.SUBSTR("apples", "5", "1");
    }
    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFail2() {
        StandardLibrary.SUBSTR("apples", "a", "1");
    }
    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFail3() {
        StandardLibrary.SUBSTR("apples", "5", "-1");
    }
    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFail4() {
        StandardLibrary.SUBSTR("apples", "5", "b");
    }
    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFail5() {
        StandardLibrary.SUBSTR("apples", "5", "1000");
    }
    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFail6() {
        StandardLibrary.SUBSTR("apples", "-1", "1000");
    }

    @Test(expected = FunctionResultException.class)
    public void TestSUBSTRFails7() {
        StandardLibrary.SUBSTR("apples", "1000", "1001");
    }

    @Test
    public void TestPrint() {
        StandardLibrary.PRINT("no point of this");
    }

    @Test
    public void TestTRUE() {
        assertEquals("true", StandardLibrary.TRUE());
    }

    @Test
    public void TestFalse() {
        assertEquals("false", StandardLibrary.FALSE());
    }

    @Test
    public void TestAFTER() {
        assertEquals("anemail.com", StandardLibrary.AFTER("thisis@anemail.com", "@"));
        assertEquals("", StandardLibrary.AFTER("thisis@", "@"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestAFTERFail() {
        StandardLibrary.AFTER("addfjhfj", "@");
    }

    @Test
    public void TestBEFORE() {
        assertEquals("thisis", StandardLibrary.BEFORE("thisis@anemail.com", "@"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestBEFOREFail() {
        StandardLibrary.BEFORE("adasdasd", "@");
    }

    @Test
    public void TestHAS() {
        assertTrue(StandardLibrary.HAS("apples", "ppl"));
        assertFalse(StandardLibrary.HAS("apples", "z"));
    }

    @Test
    public void TestDATE() {
        String ret = StandardLibrary.DATE("M/yyyy/d", "yyyy/MM/dd", "05/2019/05");

        assertEquals("2019/05/05", ret);
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATEFailA() {
        StandardLibrary.DATE("M/yyyy/d", "yyyy/MM/dd", "ABCD");
    }
    
    @Test
    public void TestDATEADD() {
        String ret = StandardLibrary.DATEADD("yyyy/MM/dd", "2019/05/05", "20", "0", "0");
        assertEquals("2019/05/25", ret);
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATEADDFailA() {
        String ret = StandardLibrary.DATEADD("yyyy/MM/dd", "2a19/05/05", "20", "0", "0");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATEADDFailB() {
        String ret = StandardLibrary.DATEADD("yyyy/MM/dd", "2019/05/05", "2a", "0", "0");
    }

    @Test
    public void TestDATESUB() {
        String ret = StandardLibrary.DATESUB("yyyy/MM/dd", "2019/05/05", "20", "0", "0");
        assertEquals("2019/04/15", ret);
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATESUBFailA() {
        String ret = StandardLibrary.DATESUB("yyyy/MM/dd", "2a19/05/05", "20", "0", "0");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATESUBFailB() {
        String ret = StandardLibrary.DATESUB("yyyy/MM/dd", "2019/05/05", "2a", "0", "0");
    }

    @Test
    public void TestDATECMP() {
        int ret = StandardLibrary.DATECMP("yyyy/MM/dd", "2018/05/05", "2017/05/05");

        assertEquals(1, ret);
        
        ret = StandardLibrary.DATECMP("yyyy/MM/dd", "2017/05/05", "2017/05/05");
        assertEquals(0, ret);

        ret = StandardLibrary.DATECMP("yyyy/MM/dd", "2017/05/05", "2019/05/05");
        assertEquals(-1, ret);
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATECMPFailA() {
        StandardLibrary.DATECMP("yyyy/MM/dd", "2501a2/05/05", "2002/05/05");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATECMPFailB() {
        StandardLibrary.DATECMP("yyyy/MM/dd", "2502/05/05", "2002/0a/05");
    }

    @Test
    public void TestDATEPREV() {
        String ret = StandardLibrary.DATEPREV("yyyy/MM/dd", "2019/07/02", "3"); 
        assertEquals("2019/06/26", ret);


    }

    @Test(expected = FunctionResultException.class)
    public void TestDATEPREVFailA() {
        StandardLibrary.DATEPREV("yyyy/MM/dd", "20a9/07/02", "3");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATEPREVFailB() {
        StandardLibrary.DATEPREV("yyyy/MM/dd", "2019/07/02", "7");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATEPREVFailC() {
        StandardLibrary.DATEPREV("yyyy/MM/dd", "2019/07/02", "7a");
    }

    @Test
    public void TestDATENEXT() {
        String ret = StandardLibrary.DATENEXT("yyyy/MM/dd", "2019/07/02", "3"); 
        assertEquals("2019/07/03", ret);


    }

    @Test(expected = FunctionResultException.class)
    public void TestDATENEXTFailA() {
        StandardLibrary.DATENEXT("yyyy/MM/dd", "20a9/07/02", "3");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATENEXTFailB() {
        StandardLibrary.DATENEXT("yyyy/MM/dd", "2019/07/02", "7");
    }

    @Test(expected = FunctionResultException.class)
    public void TestDATENEXTFailC() {
        StandardLibrary.DATENEXT("yyyy/MM/dd", "2019/07/02", "7a");
    }
}
