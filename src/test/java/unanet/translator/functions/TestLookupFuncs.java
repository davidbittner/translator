package unanet.translator.functions;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.exceptions.FunctionResultException;

import static org.junit.Assert.*;

public class TestLookupFuncs {
    final String filename = "testfile";

    @Before
    public void SetUp() {
        LookupFuncs.files = null;

        String importFile = getClass().getResource("/resources/import_short").getPath();
        LookupFuncs.LOAD(importFile, filename);
    }

    @Test
    public void TestCheckExistence() {
        LookupFuncs.checkExistence(filename);
    }

    @Test(expected = FunctionResultException.class)
    public void TestCheckExistenceFail() {
        LookupFuncs.files = null;
        LookupFuncs.checkExistence(filename + "a");
    }

    @Test(expected = FunctionResultException.class)
    public void TestCheckExistenceFail2() {
        LookupFuncs.checkExistence("asdsad");
    }

    @Test
    public void TestLOAD() {
        assertNotNull(LookupFuncs.files.get(filename));
        assertNotNull(LookupFuncs.files.get(filename).content);
        assertNotNull(LookupFuncs.files.get(filename).headers);

        assertArrayEquals(new String[]{"Name","Age","Job"}, LookupFuncs.files.get(filename).headers);
        assertEquals(3, LookupFuncs.files.get(filename).content.size());
        assertEquals(3, LookupFuncs.files.get(filename).content.get(0).length);
    }

    @Test(expected = FunctionResultException.class)
    public void TestLOADFailDuplicate() {
        String importFile = getClass().getResource("/resources/import_short").getPath();
        LookupFuncs.LOAD(importFile, filename);
    }

    @Test(expected = FunctionResultException.class)
    public void TestLOADFail() {
        LookupFuncs.LOAD(filename, "fail");
    }

    @Test
    public void TestEXISTS() {
        assertTrue(LookupFuncs.EXISTS(filename, "Name", "David"));
        assertFalse(LookupFuncs.EXISTS(filename, "Name", "Jennifer"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestEXISTSFails() {
        LookupFuncs.EXISTS(filename, "Doesn'tExist", "Blah");
    }

    @Test
    public void TestLOOKUP() {
        assertEquals("19", LookupFuncs.LOOKUP(filename, "Name", "David", "Age"));
    }

    @Test
    public void TestCOUNT() {
        assertEquals(1, LookupFuncs.COUNT(filename, "Name", "David"));
    }

    @Test(expected = FunctionResultException.class)
    public void TestLOOKUPFails() {
        LookupFuncs.LOOKUP(filename, "Name", "NotThere", "Age");
    }

    @Test
    public void TestLOADMIN() {
        LookupFuncs.LOADMIN(getClass().getResource("/resources/import_short").getPath(), "imp", "3");
        LookupFuncs.files = null;
    }

    @Test(expected = FunctionResultException.class)
    public void TestLOADMINFail() {
        LookupFuncs.LOADMIN(getClass().getResource("/resources/import_short").getPath(), "imp", "30");
        LookupFuncs.files = null;
    }

    @Test
    public void TestLOADRELATIVEMIN() {
        LookupFuncs.LOADRELATIVEMIN(getClass().getResource("/resources/import_short").getPath(), "imp", "5");
        LookupFuncs.files = null;
    }

    @Test(expected = FunctionResultException.class)
    public void TestLOADRELATIVEMINFail() {
        LookupFuncs.LOADRELATIVEMIN(getClass().getResource("/resources/import_short").getPath(), "imp", "-5");
        LookupFuncs.files = null;
    }
}
