package unanet.translator.functions;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Translator;
import unanet.translator.ast.ParserHelpers;

import java.io.*;

import static org.junit.Assert.*;

public class TestGroupTotal {

	@Before
	public void setUp() {
	    //This is done to get code coverage for the header of the class.
        //Annoying.
	    new GroupTotal();
		Translator.Reset();

		GroupTotal.columns = null;
		GroupTotal.totals = null;
	}

	@Test
    public void TestGROUPTOTAL() {
	    final String id  = "testid";
	    final String id2 = "testid2";

	    assertFalse(GroupTotal.WasCalled());

	    GroupTotal.GROUPTOTAL(id);

	    for(int i = 0; i < 10; i++) {
            GroupTotal.GROUPTOTAL(id2);
        }

        var hasha = id + Translator.GetCurrentColNum();
        var hashb = id2 + Translator.GetCurrentColNum();

        int a = GroupTotal.totals.get(hasha);
	    assertEquals(1, a);

	    int b = GroupTotal.totals.get(hashb);
	    assertEquals(10, b);

	    assertTrue(GroupTotal.WasCalled());

	    assertNull(GroupTotal.totals.get("test13"));
	}

    @Test
    public void TestWriteTotals() {
	    Translator.Reset();
	    GroupTotal.totals = null;
	    GroupTotal.columns = null;

        String short_import = ParserHelpers.FileToString(
                new InputStreamReader(
                        getClass().getResourceAsStream("/resources/import_short")));

        StringReader reader   = new StringReader(short_import);
        StringWriter writer   = new StringWriter();

        Reader template = new InputStreamReader(
                getClass().getResourceAsStream("/resources/template_grouptotal"));

        Translator.LexTemplate(template);
        Translator.ProcessFile(reader, writer);

        assertNotNull(GroupTotal.totals);
        assertNotNull(GroupTotal.columns);

        reader.close();
        reader = new StringReader(writer.toString());

        try{
            writer.close();
        }catch(IOException ex) {
            ex.printStackTrace();
        }

        String comp = "1,19,1,1,2\n" +
                      "1,19,1,1,2\n" +
                      "1,23,1,1,1\n";

        writer = new StringWriter();

        GroupTotal.WriteTotals(reader, writer);
        assertEquals(comp, writer.toString());
    }
}
