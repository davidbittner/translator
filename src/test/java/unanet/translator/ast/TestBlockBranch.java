package unanet.translator.ast;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Code;
import unanet.translator.exceptions.TokenException;

import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestBlockBranch {
    private ArrayList<Token> tokens_good;
    private ArrayList<Token> tokens_bad;

    @Before
    public void setUp() {
        String tokens_good_str = ParserHelpers.FileToString(
                new InputStreamReader(this.getClass().getResourceAsStream("/resources/token_block_a")));

        String tokens_bad_str  = ParserHelpers.FileToString(
                new InputStreamReader(this.getClass().getResourceAsStream("/resources/token_block_b")));

        Code temp = new Code();
        temp.AddLine(tokens_good_str);
        tokens_good = temp.Tokenize(0);

        temp = new Code();
        temp.AddLine(tokens_bad_str);
        tokens_bad = temp.Tokenize(0);
    }

    @Test
    public void TestConstructor() {
        BlockBranch branch = new BlockBranch(tokens_good);
        String to_string = "TRUE()\n" +
                "\"Test\"\n" +
                "SETVAR(\"A\", \"B\")";

        assertEquals(to_string, branch.toString());
    }

    @Test(expected = TokenException.class)
    public void TestConstructorFail() {
        new BlockBranch(tokens_bad);
    }

    @Test
    public void TestToString() {
        BlockBranch branch = new BlockBranch(new ArrayList<>());
        assertTrue(branch.toString().isEmpty());
    }

    @Test
    public void TestProc() {
        BlockBranch branch = new BlockBranch(tokens_good);
        assertEquals("Test", branch.proc());
    }
}
