package unanet.translator.ast;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Code;
import unanet.translator.exceptions.TokenException;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestFunctionBranch {
    FunctionBranch tester_var;
    FunctionBranch tester_nonvar;

    @Before
    public void SetUp() {
        Code temp = new Code();
        temp.AddLine("CONCAT(\"A\", \"B\")");

        tester_var = new FunctionBranch(temp.Tokenize(0));

        temp = new Code();
        temp.AddLine("UPPER(CONCAT(\"A\",\"B\",\"C\"))");

        tester_nonvar = new FunctionBranch(temp.Tokenize(0));

    }

    @Test
    public void TestFunc() {
        assertEquals("CONCAT", tester_var.me.text);
        assertEquals("UPPER", tester_nonvar.me.text);
        assertTrue(tester_var.variadic);
        assertFalse(tester_nonvar.variadic);
    }

    @Test
    public void TestProc() {
        assertEquals("AB", tester_var.proc());
        assertEquals("ABC", tester_nonvar.proc());
    }

    @Test
    public void TestToString() {
        assertEquals("CONCAT(\"A\", \"B\")", tester_var.toString());
        assertEquals("UPPER(CONCAT(\"A\", \"B\", \"C\"))", tester_nonvar.toString());
    }

    @Test(expected = TokenException.class)
    public void TestParamError() {
        Code temp = new Code();
        temp.AddLine("UPPER(20, 21, 23)");

        FunctionBranch error = new FunctionBranch(temp.Tokenize(0));
    }

    @Test(expected = TokenException.class)
    public void TestFuncNotFound() {
        Code temp = new Code();
        temp.AddLine("UPP(12, 3)");

        FunctionBranch error = new FunctionBranch(temp.Tokenize(0));
    }

    @Test(expected = TokenException.class)
    public void TestFuncInvalidParams() {
        Code temp = new Code();
        temp.AddLine("NOT(5)");

        FunctionBranch func = new FunctionBranch(temp.Tokenize(0));
        func.proc();
    }
}
