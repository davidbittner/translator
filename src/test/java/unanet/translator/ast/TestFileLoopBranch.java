package unanet.translator.ast;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Code;
import unanet.translator.Translator;
import unanet.translator.functions.LookupFuncs;

import java.io.InputStreamReader;
import java.io.Reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestFileLoopBranch {
    Code block;

    @Before
    public void setUp() {
        this.block = new Code();
        Reader template = new InputStreamReader(this.getClass().getResourceAsStream("/resources/template_fileloop"));
        this.block.AddLine(ParserHelpers.FileToString(template));
        assertTrue(this.block.Lex(0));

        LookupFuncs.files = null;
        String importFile = getClass().getResource("/resources/import_short")
            .getPath();
        LookupFuncs.LOAD(importFile, "LOADED");

        Translator.Reset();
    }

    @Test
    public void TestLoop() {
        Translator.SetCurrentRow(new String[]{"a", "b", "c"});
        assertEquals(",Programmer,Marine Biologist", this.block.Execute());
    }
}
