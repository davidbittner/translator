package unanet.translator.ast;

import org.junit.Test;
import unanet.translator.Code;
import unanet.translator.exceptions.TokenException;

import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestParserHelpers {
    ArrayList<Token> getTokens(String str) {
        Code tokenizer = new Code();
        tokenizer.AddLine(str);
        return tokenizer.Tokenize(0);
    }

    @Test
    public void TestFindIf() {
        String initial_load = ParserHelpers.FileToString(
                new InputStreamReader(this.getClass().getResourceAsStream("/resources/token_parsers")));

        String []blocks = initial_load.split("~");
        assertEquals(4, blocks.length);

        ArrayList<Token> tokens = getTokens(blocks[0]);
        assertEquals(22, ParserHelpers.FindIf(tokens, 0));

        try{
            tokens = getTokens(blocks[1]);
            ParserHelpers.FindIf(tokens, 0);

            fail("Didn't throw exception");
        }catch(TokenException ex) {
        }

        try{
            tokens = getTokens(blocks[2]);
            ParserHelpers.FindIf(tokens, 0);

            fail("Didn't throw exception");
        }catch(TokenException ex) {
        }

        try{
            tokens = getTokens(blocks[3]);
            ParserHelpers.FindIf(tokens, 0);

            fail("Didn't throw exception");
        }catch(TokenException ex) {
        }
    }
}
