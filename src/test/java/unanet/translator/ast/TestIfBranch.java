package unanet.translator.ast;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Code;
import unanet.translator.Translator;

import java.io.InputStreamReader;
import java.io.Reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestIfBranch {
    Code if_block;

    @Before
    public void setUp()
    {
        if_block = new Code();

        Reader template = new InputStreamReader(this.getClass().getResourceAsStream("/resources/template_if"));
        if_block.AddLine(ParserHelpers.FileToString(template));
        assertTrue(if_block.Lex(0));

        Translator.Reset();
    }

    @Test
    public void TestIf()
    {
        Translator.SetCurrentRow(new String[]{"A","B","true"});
        assertEquals("A", if_block.Execute());

        Translator.SetCurrentRow(new String[]{"A","B","false"});
        assertEquals("B", if_block.Execute());

        Translator.SetCurrentRow(new String[]{"A","A","false"});
        assertEquals("AA", if_block.Execute());
    }

    @Test
    public void TestToString() {
        String comp = "IF(C(3))\n" +
                "{\n" +
                "\tC(1)\n" +
                "}\n" +
                "ELSEIF(EQUAL(C(1), C(2)))\n" +
                "{\n" +
                "\tCONCAT(C(1), C(2))\n" +
                "}\n" +
                "ELSE\n" +
                "{\n" +
                "\tC(2)\n" +
                "}\n";

        assertEquals(comp, if_block.toString());
    }
}
