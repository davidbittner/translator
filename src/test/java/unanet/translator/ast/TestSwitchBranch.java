package unanet.translator.ast;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.Code;
import unanet.translator.Translator;

import java.io.InputStreamReader;
import java.io.Reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSwitchBranch {
	
	Code block;
	
	@Before
	public void setUp() {
		Reader template = new InputStreamReader(this.getClass().getResourceAsStream("/resources/template_switch"));

		String template_str = ParserHelpers.FileToString(template);
		block = new Code();
		block.AddLine(template_str);
		assertTrue(block.Lex(0));
	}

	@Test
	public void TestProc()
	{
		Translator.SetCurrentRow(new String[] {"David", "19", "Programmer"});
		String output = block.Execute();
		assertEquals("David", output);
		
		Translator.SetCurrentRow(new String[] {"Madii", "19", "Programmer"});
		output = block.Execute();
		assertEquals("19", output);
		
		Translator.SetCurrentRow(new String[] {"Apple", "19", "Programmer"});
		output = block.Execute();
		assertEquals("Programmer", output);
	}

	@Test
    public void TestToString() {
	    String comp = "SWITCH(C(1))\n" +
                "{\n" +
                "\tCASE \"David\"\n" +
                "\t{\n" +
                "\t\tC(1)\t\n" +
                "\t}\n" +
                "\tCASE \"Madii\"\n" +
                "\t{\n" +
                "\t\tC(2)\t\n" +
                "\t}\n" +
                "}\n";

	    assertEquals(comp, block.toString());
    }
}
