package unanet.translator;

import org.junit.Test;
import unanet.translator.ast.Token;
import unanet.translator.exceptions.ErrorFormatter;

import static org.junit.Assert.assertEquals;

public class TestErrorFormatter {
    @Test
    public void TestFormatError() {
        String expected = "ERROR: line 1 character 1\n" +
                        "This error occurred while at row: '0', column: '0'\n" +
                        "Message:\n" +
                        "\tTest Message\n";

        Token tok = new Token("FUNC", 1, 1);
        assertEquals(expected, ErrorFormatter.FormatError(tok, "Test Message"));
    }
}
