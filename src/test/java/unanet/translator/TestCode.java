package unanet.translator;

import org.junit.Before;
import org.junit.Test;
import unanet.translator.ast.ParserHelpers;
import unanet.translator.ast.Token;
import unanet.translator.exceptions.ParsingException;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public class TestCode {
    Reader good_template;
    Reader bad_template;

    String good;
    String bad;

    Code tester;

    @Before
    public void SetUp() {
        good_template = new InputStreamReader(getClass().getResourceAsStream("/resources/template_lexing_good"));
        bad_template  = new InputStreamReader(getClass().getResourceAsStream("/resources/template_lexing_bad"));

        good = ParserHelpers.FileToString(good_template);
        bad = ParserHelpers.FileToString(bad_template);

        tester = new Code();
    }

    @Test
    public void TestIsEmpty() {
        assertTrue(tester.isEmpty());
    }

    @Test
    public void TestTokenize() {
        String []expected = new String[]{
                "LOWER",
                "(",
                "\"Testing\"",
                ")",
                "CONCAT",
                "(",
                "\"Test\"",
                ",",
                "2",
                ")",
                "<=",
                "!=",
                "<",
                "=",
                "UPPER",
                "(",
                "\"Testing\"",
                ")"
        };

        String token_test = ParserHelpers.FileToString(new InputStreamReader(getClass().getResourceAsStream("/resources/token_test")));
        tester.AddLine(token_test);
        ArrayList<Token> tokens = tester.Tokenize(0);
        String []tok_names = new String[tokens.size()];

        for(int i = 0; i < tokens.size(); i++) {
            tok_names[i] = tokens.get(i).text;
        }

        assertArrayEquals(expected, tok_names);
    }

    @Test
    public void TestLex() {
        tester.code = good;
        assertTrue(tester.Lex(0));
    }

    @Test(expected = ParsingException.class)
    public void TestLexFail() throws ParsingException {
        tester.code = bad;
        tester.Lex(0);
    }

    @Test
    public void TestExecute() {
        tester.code = good;
        tester.Lex(0);
    }

    @Test
    public void TestSubBackwardsCompat() {
        var test_template = new InputStreamReader(getClass().getResourceAsStream("/resources/backwards_compat_sub"));
        tester.AddLine(ParserHelpers.FileToString(test_template));
        var tokens = tester.Tokenize(0);

        tester.SubForBackwardsCompat(tokens);
    }
}
