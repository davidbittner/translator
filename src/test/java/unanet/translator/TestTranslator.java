package unanet.translator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import unanet.translator.ast.ParserHelpers;
import unanet.translator.exceptions.ErrorFormatter;
import unanet.translator.exceptions.TokenException;

import java.io.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TestTranslator {   
    private Reader template;
    private Reader long_template;
    private Reader fixed_template;
    private Reader short_import;
    private Reader long_import;
    private Reader fixed_import;
    private Writer export_file;
    
    @Before
    public void setUp() {
        Translator.Reset();

        template       = new InputStreamReader(getClass().getResourceAsStream("/resources/template_short"));
        long_template  = new InputStreamReader(getClass().getResourceAsStream("/resources/template_stress"));
        short_import   = new InputStreamReader(getClass().getResourceAsStream("/resources/import_short"));
        long_import    = new InputStreamReader(getClass().getResourceAsStream("/resources/import_stress"));
        fixed_import   = new InputStreamReader(getClass().getResourceAsStream("/resources/import_fixed"));
        fixed_template = new InputStreamReader(getClass().getResourceAsStream("/resources/template_fixed"));

        export_file  = new StringWriter();

        Translator.LexTemplate(template);
    }

    @After
    public void tearDown() throws Exception {
        template.close();
        short_import.close();
        
        export_file.close();
        System.setProperty("java.util.logging.SimpleFormatter.format", "");
        Translator.Reset();
    }

    @Test
    public void TestLexTemplate() {
        assertEquals(Translator.codeBlocks.size(), 5);
    }
    
    @Test
    public void TestIterate() {
        String[] comp = {"David","David19ProgrammerDavid19Programmer","Programmer"};
        Translator.currentRow = new String[]{"David","19","Programmer"};

        String[] returned = Translator.Iterate();
        assertArrayEquals(comp, returned);
    }
    
    @Test
    public void TestProcessFile() {
        Translator.ProcessFile(short_import, export_file);
        
        String []headers = {"Name","Age","Job"};
        assertArrayEquals(headers, Translator.inputHeaders);
    }

    @Test
    public void TestStress() {
        Translator.ProcessFile(long_import, export_file);

        Translator.LexTemplate(long_template);
        Translator.ProcessFile(long_import, export_file);
    }

    @Test
    public void TestMain() {
        try{
        Translator.main(new String[]{
                "-template", getClass().getResource("/resources/template_translator").getPath(),
                "-import",   getClass().getResource("/resources/import_stress").getPath(),
                "-export",   "test_export",
                "--debug"
        });

            String output = ParserHelpers.FileToString(new FileReader("test_export"));
            new File("test_export").delete();
        } catch (FileNotFoundException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void TestMainFail() {
        Translator.main(new String[]{
                "-template", getClass().getResource("/resources/template_translator").getPath(),
                "-import",   getClass().getResource("/resources/import_short").getPath(),
                "--debug"
        });
    }

    @Test
    public void TestMainIncorrectRowCount() {
        Translator.main(new String[]{
                "-template", getClass().getResource("/resources/template_translator_bad").getPath(),
                "-import",   getClass().getResource("/resources/import_short").getPath(),
                "-export",   "test_export",
                "-log",      "log_file",
                "--debug"
        });

        try{
            ParserHelpers.FileToString(new FileReader("test_export"));
            new File("test_export").delete();
            new File("log_file").delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
