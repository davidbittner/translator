package unanet.translator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import unanet.translator.ast.ParserHelpers;
import unanet.translator.exceptions.ErrorFormatter;
import unanet.translator.exceptions.TokenException;
import unanet.translator.functions.GroupTotal;

public class Translator
{
    private static Logger logger = Logger.getGlobal();

    /**
     * The blocks of code that the template file contained.
     */
    static ArrayList<Code> codeBlocks;
    /**
     * The current row of data being manipulated
     */
    static String []currentRow;
    /**
     * The headers of the input file
     */
    static String []inputHeaders;
    /**
     * The headers to be written to the output file
     */
    private static String []outputHeaders;

    /**
     * The number of the current row of the input file
     */
    private static int currentRowNum;

    /**
     * The number of the current column being processed.
     */
    private static int currentColNum;

    /**
     * The amount of lines in the import file.
     */
    private static long importLineCount;

    /**
     * Whether or not to ignore this row of output
     */
    private static boolean ignoreRow;

    /**
     * Which row to ignore due to being LogicOnly rows.
     */
    private static boolean []logicOnly;
    /**
     * Tells whether or not the current field is logic only
     */
    private static boolean   logicOnlyFlag;

    /**
     * Tells whether or not to sort the output
     */
    private static boolean sortFlag;
    /**
     * Tells whether or not to trim all non-unique fields from the output.
     */
    private static boolean uniqueFlag;

    /**
     * Whether or not the import file has headers.
     */
    private static boolean hasImportHeaders;

    static int progress_counter;

    /**
     * Only used in tests, resets everything to defaults
     */
    public static void Reset() {
        ignoreRow        = false;
        currentRow       = null;
        importLineCount  = 0;
        currentRowNum    = 0;
        currentColNum    = 0;

        inputHeaders     = null;
        hasImportHeaders = true;
        outputHeaders    = null;

        codeBlocks = null;

        logicOnly     = null;
        logicOnlyFlag = false;

        progress_counter = 0;
    }

    /**
     * Sets the LogicOnly flag to true to set this row as being for logic, only.
     */
    public static void LogicOnly() {
        logicOnlyFlag = true;
    }

    /**
     * Tells whether or not there are import headers. This means there are headers on the file being imported.
     * @return Whether or not there are import headers
     */
    public static boolean CheckImportHeaders() {
        return hasImportHeaders;
    }

    /**
     * Tells whether or not there are no import headers.
     */
    public static void IgnoreImportHeaders() {
        hasImportHeaders = false;
    }

    /**
     * Tells the Translator to sort the output on exit
     */
    public static void SortOutput() {
        sortFlag = true;
    }

    /**
     * Tells the Translator to only output uniques after sorting.
     */
    public static void OnlyUniques() {
        uniqueFlag = true;
    }

    /**
     * Returns the list of which columns are logic only columns
     * @return Returns a list of booleans telling which columns are logic only
     */
    public static boolean[] CheckLogicOnlyList() {
        return logicOnly.clone();
    }

    /**
     * Returns the current column number. This is used for GroupTotal
     * @return Returns the current column number
     */
    public static int GetCurrentColNum() {
        return currentColNum;
    }

    /**
     * Gets the current column number that is being processed.
     * @return The index of the current column number.
     */
    public static long GetImportLineCount() { return importLineCount; }

    /**
     * Lexes the template file that can be read from the given reader
     * @param templateFile The reader that the file should be read from
     */
    public static void LexTemplate(final Reader templateFile)
    {
        final ArrayList<Code> code_chunks = new ArrayList<>();

        boolean failed = false;

        try {
            final BufferedReader templateReader = new BufferedReader(templateFile);
            String line;
            
            Code curBlock = new Code();

            //These are for finding out what line errors occured on
            //This is so each code block knows where it is relative to the start of the file
            int last_start = 0;
            int cur_line = 0;
            while((line = templateReader.readLine()) != null)
            {
                if(line.equals("END"))
                {
                    try {
                        curBlock.Lex(last_start);
                        code_chunks.add(curBlock);
                    }catch(final TokenException ex) {
                        logger.severe(ErrorFormatter.FormatError(ex.failedAt, ex.getMessage()));
                        failed = true;
                    }

                    curBlock = new Code();
                    last_start = cur_line;
                }else {
                    curBlock.AddLine(line);
                }
                cur_line++;
            }
            if(!curBlock.isEmpty())
            {
                curBlock.Lex(last_start);
                code_chunks.add(curBlock);
            }
            
            templateReader.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }

        if(failed) {
            codeBlocks = null;
            throw new RuntimeException("Failed to parse/lex the template file.");
        }

        codeBlocks = code_chunks;
    }

    /**
     * Checks if the entire row itself should be ignored.
     * @return Returns a boolean that says whether or not this row is going to be ignored.
     */
    public static boolean CheckIgnore()
    {
        return ignoreRow;
    }
    
    /**
     * Performs an iteration with all of the given blocks
     * @return The array of output that each block gave
     */
    static String[] Iterate()
    {
        //This will happen in most tests, but will not happen during normal execution.
        if(logicOnly == null) {
            logicOnly = new boolean[codeBlocks.size()];
        }

        final String []ret = new String[codeBlocks.size()];

        currentColNum = 0;
        for(final Code block : codeBlocks)
        {
            ret[currentColNum] = block.Execute();
            if(!logicOnly[currentColNum]) {
                logicOnly[currentColNum] = logicOnlyFlag;
            }

            logicOnlyFlag = false;
            currentColNum++;

            if(ignoreRow) {
                return null;
            }
        }

        final ArrayList<String> write = new ArrayList<>();
        write.ensureCapacity(logicOnly.length);

        for(int i = 0; i < logicOnly.length; i++) {
            if(!logicOnly[i]) {
                write.add(ret[i]);
            }
        }
        
        return write.toArray(new String[0]);
    }
    
    /**
     * Processes the input file, writes to the output file
     * @param import_stream The stream of the file being read
     * @param export_stream The stream of the file being written to
     */
    public static void ProcessFile(final Reader import_stream, final Writer export_stream)
    {
        final ProgressCounter prog_counter = new ProgressCounter();

        try {
            // The input does not properly handle backslashes. A custom reader must be
            // constructed to properly handle backslashes and make double quotes the escape character.
            final RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().build();
            final CSVReader import_reader = new CSVReaderBuilder(import_stream)
                .withCSVParser(rfc4180Parser)
                .build();

            final CSVWriter export_writer = new CSVWriter(export_stream);

            if(hasImportHeaders) {
                inputHeaders = import_reader.readNext();
            }

            currentRowNum = 0;
            prog_counter.start();
            while((currentRow = import_reader.readNext()) != null)
            {
                progress_counter++;

                final String []ret = Iterate();
                if(ignoreRow)
                {
                    ignoreRow = false;
                    continue;
                }

                if(hasImportHeaders) {
                    if(inputHeaders.length != currentRow.length)
                    {
                        export_writer.close();
                        throw new RuntimeException("Input headers do not match the amount of columns being read.");
                    }
                }

                if(outputHeaders != null) {
                    if(ret.length != outputHeaders.length) {
                        export_writer.close();
                        throw new RuntimeException(String.format(
                                "Output headers do not match the amount of columns outputted. Has '%d' needs '%d'",
                                ret.length,
                                outputHeaders.length));
                    }
                }

                currentRowNum++;
                export_writer.writeNext(ret, false);
            }

            export_writer.close();
        } catch (final IOException e) {
            e.printStackTrace();
        } catch(final Exception ex) {
            prog_counter.quit();
            throw ex;
        } finally {
            prog_counter.quit();
        }

        prog_counter.quit();
    }
    /**
     * Gets the current row being parsed, for use of the C() and FC() function.
     * @return The array of the current row
     */
    public static String[] GetCurrentRow() {
        return (currentRow == null)?(null):(currentRow.clone());
    }
    /**
     * A flag for ignoring a row or not.
     */
    public static void IgnoreRow()
    {
        ignoreRow = true;
    }

    /**
     * Sets the output headers to the given array. This is used by the HEADER function.
     * @param headers What to set the output headers to.
     */
    public static void SetOutputHeaders(final String[] headers)
    {
        outputHeaders = headers.clone();
    }

    /**
     * Returns the given headers.
     * @return The headers that have been passed in through the HEADER function.
     */
    public static String[] CheckHeaders() {
        return outputHeaders;
    }

    /**
     * Gets the number of the current row being processed, used by SEQ()
     * @return The current row being processed
     */
    public static int GetCurrentRowNum()
    {
        return currentRowNum;
    }

    /**
     * Sets the current row. This is really only used for testing.
     * @param cont What to set the current row to.
     */
    public static void SetCurrentRow(final String[] cont)
    {
        currentRow = cont;
    }

    /**
     * Sorts the output rows. This is always invoked if Unique is called
     * @param read The input buffer to read from.
     * @param write The output buffer to write the sorted fields to.
     */
    private static void SortFields(final BufferedReader read, final Writer write) {
        final ArrayList<String> buffer = new ArrayList<>();

        String line;
        try{
            String headers;
            if(hasImportHeaders) {
                headers = read.readLine();
                write.write(headers + "\n");
            }
            while((line = read.readLine()) != null) {
                buffer.add(line);
            }
        }
        catch(final IOException ex) {
            logger.severe("Failed to read from BufferedReader:\n+");
        }

        buffer.sort(String::compareTo);

        for(final String lin : buffer) {
            try {
                write.write(lin + "\n");
            }catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Trims all non-unique rows.
     * @param read The buffer to read from (the input file)
     * @param write Where to write to (the output file)
     */
    private static void UniqueFields(final BufferedReader read, final Writer write) {
        try{
            String line;
            if(hasImportHeaders) {
                //get headers
                line = read.readLine();
                write.write(line + "\n");
            }

            String prev = null;


            while((line = read.readLine()) != null) {
                if(!line.equals(prev)) {
                    write.write(line + "\n");
                    prev = line;
                }
            }
        }
        catch(final IOException ex) {
            logger.severe("Failed to read from BufferedReader");
        }
    }

    /**
     * Starts the translation invoked from the command line.
     * @param args The command-line arguments passed into the translator.
     */
    public static void main( final String args[] )
    {
        final Formatter log_formatter = new Formatter() {
            @Override
            public String format(final LogRecord record) {
                final StringBuilder text = new StringBuilder();

                if(record.getLevel().intValue() <= Level.INFO.intValue()) {
                    text.append(record.getLevel().toString());
                    text.append(" ");
                    text.append(LocalDateTime.now().toString());
                    text.append(" : ");
                }

                text.append(record.getMessage());
                text.append("\n");

                return text.toString();
            }
        };

        final Options options = new Options();

        options.addRequiredOption("template", null, true, "The template script to operate with");
        options.addRequiredOption("import",   null, true, "The import file to operate on");
        options.addRequiredOption("export", null, true, "The export file to write results to");
        options.addOption("log", null,true, "The file to log to");

        options.addOption("d", "debug", false, "Disables all logging");

        final CommandLineParser arg_parser = new DefaultParser();
        CommandLine cmd;

        final ConsoleHandler handler = new ConsoleHandler();

        logger.setUseParentHandlers(false);

        //Why?? Why would you need to set it for handlers AND loggers????
        //Why did no guide mention this???
        logger.setLevel(Level.ALL);

        handler.setLevel(Level.ALL);
        handler.setFormatter(log_formatter);

        logger.addHandler(handler);

        try {
            cmd = arg_parser.parse(options, args);
        }catch(final ParseException ex) {
            final HelpFormatter help_msg = new HelpFormatter();
            help_msg.printHelp("translator", options);
            return;
        }

        if(cmd.hasOption("debug")) {
            logger.setLevel(Level.OFF);
        }

        if(cmd.hasOption("log")) {
            try{
                final FileHandler log_to = new FileHandler(cmd.getOptionValue("log"));
                logger.addHandler(log_to);

                log_to.setLevel(Level.ALL);

                log_to.setFormatter(log_formatter);
            } catch(final IOException ex) {
                logger.severe(ex.getLocalizedMessage());
            }
        }

        final String templateFile = cmd.getOptionValue("template");
        final String dataFile     = cmd.getOptionValue("import");
        final String outputFile   = cmd.getOptionValue("export");

        try{
            importLineCount = ParserHelpers.GetLineCount(dataFile);
        } catch(final IOException ex) {
            //Will never get here, as it will have already quit if the file cannot be opened.
            importLineCount = -1;
        }

        try {
            LexTemplate(new FileReader(templateFile));
            logger.fine(String.format("Template '%s' lexed successfully.", templateFile));
        } catch (FileNotFoundException | RuntimeException e) {
            e.printStackTrace();
            logger.severe(e.getLocalizedMessage());
            return;
        }

        if(codeBlocks.size() > 1) {
            final int HEADER_ROW = 0;
            hasImportHeaders = true;

            try{
                codeBlocks.get(HEADER_ROW).Execute();
            }catch(final TokenException ex) {
                logger.severe(ErrorFormatter.FormatError(ex.failedAt, ex.getMessage()));
                return;
            }

            codeBlocks.remove(HEADER_ROW);

            logicOnly = new boolean[codeBlocks.size()];
        }else{
            logger.severe(String.format("Must have more than one code block, you have '%s'", codeBlocks.size()));
            return;
        }

        try {
            logger.fine("Starting translation...");
            Reader in = new FileReader(dataFile);
            Writer out = new FileWriter(outputFile);

            //Print headers, if they exist
            if(outputHeaders != null ) {
                outputHeaders[0] = "*" + outputHeaders[0];

                final CSVWriter out_writer = new CSVWriter(out);
                out_writer.writeNext(outputHeaders, false);

                out_writer.close();
            }

            try{
                ProcessFile(in, out);
            }catch(final TokenException ex) {
                logger.severe(ErrorFormatter.FormatError(ex.failedAt, ex.getLocalizedMessage()));

                return;
            }catch(final RuntimeException ex) {
                logger.severe(ex.getLocalizedMessage());
                return;
            }

            in.close();
            out.close();

            logger.fine(String.format("Finished writing to the output file '%s'", outputFile));

            //If asked to be sorted then it sorts,
            //but since filtering uniques requires sorting, sort in that case as well
            if(sortFlag || uniqueFlag) {
                logger.fine("Beginning to sort output...");

                in  = new FileReader(outputFile);
                out = new StringWriter();

                SortFields(new BufferedReader(in), out);

                in.close();

                FileWriter toFile = new FileWriter(outputFile);
                toFile.write(out.toString());
                toFile.close();
                out.close();

                logger.fine("Sorting done.");

                if(uniqueFlag) {
                    logger.fine("Pruning non-unique fields...");
                    in  = new FileReader(outputFile);
                    out = new StringWriter();

                    UniqueFields(new BufferedReader(in), out);

                    in.close();

                    toFile = new FileWriter(outputFile);
                    toFile.write(out.toString());
                    toFile.close();
                    out.close();

                    logger.fine("Finished pruning non-unique fields.");
                }
            }

            //If GroupTotal was used, then replace the hashes
            // with the corresponding occurrence count
            if(GroupTotal.WasCalled()) {
                in  = new FileReader(outputFile);
                out = new StringWriter();

                GroupTotal.WriteTotals(new BufferedReader(in), out);

                in.close();

                final FileWriter toFile = new FileWriter(outputFile);
                toFile.write(out.toString());
                out.close();
                toFile.close();
            }

            logger.fine(String.format("Execution finished successfully with '%d' lines of output.", GetCurrentRowNum()));
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}

/**
 * This is used only to print progress on the translation file.
 */
class ProgressCounter implements Runnable {
    private static Logger logger = Logger.getGlobal();

    private Thread thr;
    private final UUID name;

    private boolean stop;

    ProgressCounter() {
        name = UUID.randomUUID();
        stop = false;
    }

    /**
     * Begins printing the current progress of the translation every $WAIT_TIME milliseconds.
     */
    public void run() {
        final long WAIT_TIME = 200;
        try{
            while(Translator.progress_counter+1 < Translator.GetImportLineCount() && !stop) {
                if(Translator.GetCurrentRowNum() != 0) {
                    double percent = (double)Translator.progress_counter/(double)Translator.GetImportLineCount();
                    percent *= 100;

                    logger.fine(String.format("Progress: %.2f%%", percent));
                }

                Thread.sleep(WAIT_TIME);
            }
        }catch(final InterruptedException ex) {
            logger.severe(ex.getLocalizedMessage());
        }
    }

    public void quit() {
        stop = true;
    }

    void start() {
        if(thr == null) {
            thr = new Thread(this, name.toString());
            thr.start();
        }
    }
}
