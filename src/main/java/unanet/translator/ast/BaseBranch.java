package unanet.translator.ast;

public abstract class BaseBranch {
    /**
     * Processes the chunk and returns a string
     * This may be the return value of a function, or a single parameter
     * @return The result of whatever functionality was passed to the branch
     */
    abstract public String proc();
}
