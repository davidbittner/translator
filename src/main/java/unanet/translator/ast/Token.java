package unanet.translator.ast;

public class Token {
    public int character;
    public int line;
    public String text;

    public Token(String text, int line, int character) {
        this.character = character;
        this.text = text;
        this.line = line;
    }

    @Override
    public String toString() {
        return text + " l:" + line + " c:" + character;
    }
}
