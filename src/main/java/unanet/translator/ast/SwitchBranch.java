package unanet.translator.ast;

import unanet.translator.exceptions.ParsingException;
import unanet.translator.exceptions.TokenException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SwitchBranch extends BaseBranch {
    HashMap<String, BlockBranch> cases;
    BaseBranch cond;
    BlockBranch def;

    /**
     * Initializes the switch branch, passing a list of tokens.
     * @param tokens The list of tokens.
     * @throws ParsingException In case of some parsing error, such as a malformed switch, it will throw an exception.
     */
    SwitchBranch(List<Token> tokens) throws ParsingException {
        def = null;

        cases = new HashMap<>();

        int cond_block = ParserHelpers.FindMatching(tokens, 0, "(", ")");
        if(ParserHelpers.IsLiteral(tokens.get(1).text))
        {
            cond = new ParameterBranch(tokens.get(1));
        }else {
            cond = new FunctionBranch(tokens.subList(1, cond_block));
        }
        
        for(int i = cond_block; i < tokens.size(); i++) {
            if(tokens.get(i).text.equals("CASE")) {

                String caseVal;
                if(tokens.get(++i).text.equals("(")) {
                    //Grab token
                    caseVal = tokens.get(++i).text;

                    //Skip the token and paren
                    i++;

                    if(!tokens.get(i).text.equals(")")) {
                        throw new TokenException(tokens.get(i-2), "No matching parentheses in CASE.");
                    }
                    i++;
                }else{
                    caseVal = tokens.get(i).text;
                    i++;
                }


                //Grab the block of code associated with the case
                int end;
                try {
                    end = ParserHelpers.FindMatching(tokens, i, "{", "}");
                } catch (Exception e) {
                    throw new TokenException(tokens.get(i), "No matching curly bracket to CASE block.");
                }

                BlockBranch holder = new BlockBranch(tokens.subList(i, end));

                i = end;
                
                if(caseVal.equals("DEFAULT")) {
                    def = holder;
                }else {
                    caseVal = ParserHelpers.TrimQuotes(caseVal);
                    cases.put(caseVal, holder);
                }
            }
        }
    }

    /**
     * Processes this switch block, evaluating the condition.
     * @return The value of the resulting switch statement.
     */
    @Override
    public String proc() {
        String ret = cond.proc();
        BlockBranch holder = cases.get(ret);
        
        //Run case, if there is none, run default, if there is none, return null
        if(holder != null) {
            return holder.proc();
        }else {
            if(def != null)
            {
                return def.proc();
            }else {
                return null;
            }
        }
    }

    /**
     * Converts this, and all it's children to a string.
     * @return The resulting string.
     */
    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("SWITCH(" + cond.toString() + ")\n");
        ret.append("{\n");
        Iterator it = cases.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            ret.append("\tCASE \"" + entry.getKey() + "\"\n");

            ret.append("\t{\n");
            String[] lines = (entry.getValue()).toString().split(System.lineSeparator());
            for(String line : lines) {
                ret.append("\t\t");
                ret.append(line);
                ret.append("\t\n");
            }
            ret.append("\t}\n");
        }
        ret.append("}\n");

        return ret.toString();
    }
}
