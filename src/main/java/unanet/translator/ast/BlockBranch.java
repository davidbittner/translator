package unanet.translator.ast;

import unanet.translator.exceptions.ParsingException;
import unanet.translator.exceptions.TokenException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class BlockBranch extends BaseBranch {
    static Logger logger = Logger.getGlobal();
    List<BaseBranch> children;

    Token me;

    public BlockBranch(List<Token> tokens) throws ParsingException {
        this.children = new ArrayList<>();

        if(tokens.size() > 0) {
            me = tokens.get(0);
        }

        for(int i = 0; i < tokens.size(); i++) {
            if(ParserHelpers.IsLiteral(tokens.get(i).text)) {
                children.add(new ParameterBranch(tokens.get(i)));
            }else if (tokens.get(i).text.equals("(")) {
                switch (tokens.get(i - 1).text) {
                    case "SWITCH": {
                        int end = 0;
                        try {
                            end = ParserHelpers.FindMatching(tokens, i, "(", ")");
                        } catch (Exception e) {
                            throw new TokenException(tokens.get(i-1), "No matching parentheses to SWITCH statement");
                        }
                        try {
                            end = ParserHelpers.FindMatching(tokens, end + 1, "{", "}");
                        } catch (Exception e) {
                            throw new TokenException(tokens.get(i-1), "No matching curly brackets to SWITCH statement");
                        }

                        children.add(new SwitchBranch(tokens.subList(i, end)));

                        i = end;
                        continue;
                    }
                    case "IF": {
                        int end = 0;
                        end = ParserHelpers.FindIf(tokens, i-1);

                        children.add(new IfBranch(tokens.subList(i-1, end)));

                        i = end;
                        continue;
                    }
                    case "ITERATEFILE": {
                        int end = 0;
                        end = ParserHelpers.FindMatching(tokens, i, "(", ")");
                        end = ParserHelpers.FindMatching(tokens, end+1, "{", "}");
                        children.add(
                            new FileLoopBranch(tokens.subList(i-1, tokens.size()))
                        );

                        i = end;
                        continue;
                    }
                    default: {
                        int end;
                        try {
                            end = ParserHelpers.FindMatching(tokens,
                                    i,
                                    "(",
                                    ")");
                        } catch (Exception e) {
                            Token temp = tokens.get(i-1);
                            throw new TokenException(temp, String.format("No matching parentheses found for function %s.", temp.text));
                        }

                        children.add(new FunctionBranch(tokens.subList(i - 1, end)));
                        i = end;
                        continue;
                    }
                }
            }
        }
    }

    @Override
    public String proc() {
        String ret = null;
        for(BaseBranch child : children)
        {
            String temp = child.proc();
            if(temp != null) {
                ret = temp;
            }
        }

        return ret;
    }

    @Override
    public String toString() {
        if(children.size() == 0) {
            return "";
        }

        StringBuilder ret = new StringBuilder(children.get(0).toString());
        for(int i = 1; i < children.size(); i++) {
            ret.append("\n" + children.get(i).toString());
        }

        return ret.toString();
    }

}
