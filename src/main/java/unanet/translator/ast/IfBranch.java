package unanet.translator.ast;

import unanet.translator.exceptions.ParsingException;
import unanet.translator.exceptions.TokenException;

import java.util.ArrayList;
import java.util.List;

public class IfBranch extends BaseBranch {
    class LogicBranch {
        LogicBranch(BaseBranch evaluator, BlockBranch code) {
            this.evaluator = evaluator;
            this.code = code;
        }

        BaseBranch evaluator;
        BlockBranch code;

        boolean evaluate() {
            return evaluator.proc().equals("true");
        }
    }

    ArrayList<LogicBranch> children;
    BlockBranch            else_branch;

    IfBranch(List<Token> tokens)
    {
        else_branch = null;
        children = new ArrayList<>();

        for(int i = 0; i < tokens.size(); i++)
        {
            switch(tokens.get(i).text)
            {
                case "ELSEIF" :
                case "IF": {
                    //Skip if, go to parens
                    i++;

                    BaseBranch eval;

                    int end;
                    try{
                        end = ParserHelpers.FindMatching(tokens, i, "(", ")");
                    }catch(ParsingException ex) {
                        throw new TokenException(tokens.get(i-1), "No matching parentheses to If statement.");
                    }
                    if (ParserHelpers.IsLiteral(tokens.get(i + 1).text)) {
                        //It's just a literal
                        eval = new ParameterBranch(tokens.get(i + 1));
                    } else {
                        //It's a function, not a literal, start past the paren
                        eval = new FunctionBranch(tokens.subList(i + 1, end));
                    }
                    i = end;
                    //Skip the end parentheses
                    i++;

                    end = ParserHelpers.FindMatching(tokens, i, "{", "}");
                    BlockBranch temp_block = new BlockBranch(tokens.subList(i, end));
                    i = end;

                    children.add(new LogicBranch(eval, temp_block));

                    continue;
                }
                case "ELSE": {
                    //Skip the else
                    i++;

                    int end = ParserHelpers.FindMatching(tokens, i, "{", "}");
                    else_branch = new BlockBranch(tokens.subList(i, end));

                    i = end;
                }
            }
        }
    }

    public String proc() {
        for(LogicBranch child : children) {
            if(child.evaluate()) {
                return child.code.proc();
            }
        }

        if(else_branch != null) {
            return else_branch.proc();
        }else{
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();

        for(int i = 0; i < children.size(); i++) {
            if(i == 0) {
                ret.append("IF(");
            }else {
                ret.append("ELSEIF(");
            }
            ret.append(children.get(i).evaluator.toString());
            ret.append(")\n");
            ret.append("{\n");

            String indenter = children.get(i).code.toString();
            String[] lines = indenter.split(System.lineSeparator());
            for(String line : lines) {
                ret.append("\t");
                ret.append(line);
                ret.append("\n");
            }

            ret.append("}\n");
        }

        if(else_branch != null) {
            ret.append("ELSE\n");
            ret.append("{\n");

            String indenter = else_branch.toString();
            String[] lines = indenter.split(System.lineSeparator());
            for(String line : lines) {
                ret.append("\t");
                ret.append(line);
                ret.append("\n");
            }
            ret.append("}\n");
        }

        return ret.toString();
    }
}
