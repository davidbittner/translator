package unanet.translator.ast;

import unanet.translator.exceptions.ParsingException;
import unanet.translator.exceptions.TokenException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public abstract class ParserHelpers {
    /**
     * Used to find matching parentheses, quotes, curly brackets, etc.
     * @param tokens The tokens to search through.
     * @param start The start index to search through.
     * @param up The character to increment the nesting level.
     * @param down The character to decrement the nesting level.
     * @return Returns the location the matching character.
     * @throws ParsingException Throws a parsing exception if there was no matching whatever.
     */
    static public int FindMatching(List<Token> tokens, int start, String up, String down) throws ParsingException
    {
        int layer = 1;
        if(!tokens.get(start).text.equals(up)) {
            throw new ParsingException("Expected '" + up + "' but none found.");
        }
        for(int i = start+1; layer != 0; i++)
        {
            if(i == tokens.size())
            {
                throw new ParsingException("");
            }

            if(tokens.get(i).text.equals(up))
            {
                layer++;
            }else if(tokens.get(i).text.equals(down))
            {
                layer--;
            }

            if(layer == 0)
            {
                return i;
            }
        }
        throw new ParsingException("");
    }

    /**
     * Says whether or not the given string is numerical.
     * @param str The string to checck.
     * @return 127534 returns true, 123a23 returns false.
     */
    static public boolean IsNumber(String str)
    {
        boolean one_period = false;

        int i = 0;

        if(str.isEmpty()) {
            return false;
        }

        if(str.charAt(i) == '-') {
            ++i;
        }

        for(; i < str.length(); i++)
        {
            if(!Character.isDigit(str.charAt(i)) && str.charAt(i) != '.')
            {
                return false;
            }else if(str.charAt(i) == '.') {
                if(one_period) {
                    return false;
                }else{
                    one_period = true;
                }
            }
        }
        return true;
    }

    /**
     * Cuts quotes from the start and end of literals.
     * @param str The string to be trimmed.
     * @return The trimmed string.
     */
    static public String TrimQuotes(String str)
    {
        if(str.isEmpty())
        {
            return str;
        }else if(str.charAt(0) == '"' || str.charAt(str.length()-1) == '"')
        {
            //Cut quotes
            str = str.substring(1, str.length()-1);
        }
        return str;
    }

    /**
     * Checks whether or not the given string is a literal (does it have quotes at the beginning and end)
     * @param token The token to check
     * @return Returns true or false based on whether or not it is a literal.
     */
    static public boolean IsLiteral(String token)
    {
        if(token.isEmpty())
        {
            return false;
        }else {
            return token.charAt(0) == '"' && token.charAt(token.length()-1) == '"';
        }
    }

    /**
     * Skips if blocks, because it's done often and contains a lot of boilerplate.
     * @param tokens The tokens to skip through.
     * @param start The start of the If Block.
     * @return The end token index of the end of the block.
     */
    static public int FindIf(List<Token> tokens, int start)
    {
        for(int i = start; i < tokens.size(); i++)
        {
            switch(tokens.get(i).text) {
                case "ELSEIF":
                case "IF": {
                    //Skip the if
                    i++;

                    try {
                        i = ParserHelpers.FindMatching(tokens, i, "(", ")");
                    }catch(ParsingException ex) {
                        throw new TokenException(tokens.get(i), "No matching parentheses to IF/ELSEIF statement.");
                    }
                    //Skip paren
                    i++;

                    try{
                        i = ParserHelpers.FindMatching(tokens, i, "{", "}");
                    }catch(ParsingException ex) {
                        throw new TokenException(tokens.get(i), "No matching curly brackets to IF/ELSEIF statement.");
                    }
                    continue;
                }
                case "ELSE": {
                    //Skip the else
                    i++;

                    try{
                        i = ParserHelpers.FindMatching(tokens, i, "{", "}");
                    }catch(ParsingException ex) {
                        throw new TokenException(tokens.get(i), "No matching curly brackets to ELSE statement.");
                    }
                    continue;
                }
                //End of the block
                default: {
                    return i;
                }
            }
        }
        return tokens.size();
    }

    /**
     * Reads an entire file to a string.
     * @param read The reader to read from.
     * @return The string of the contents of the file.
     */
    static public String FileToString(Reader read) {
        StringBuilder out = new StringBuilder();

        BufferedReader buff = new BufferedReader(read);
        try{
            String line;
            while((line = buff.readLine()) != null) {
                out.append(line+System.lineSeparator());
            }
        }catch(IOException ex) {
            ex.printStackTrace();
        }

        return out.toString();
    }

    /**
     * Gets the length of the file in # of lines.
     * @param filename The name of the file to check.
     * @return Returns a long that says how many lines long the file is
     * @throws IOException If the file was unable to be opened, it throws an exception.
     */
    static public long GetLineCount(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        long lines = 0;
        while (reader.readLine() != null) lines++;
        reader.close();

        return lines;
    }

    static public List<BaseBranch> parseParameters(List<Token> tokens) throws ParsingException {
        List<BaseBranch> children = new ArrayList<>();

        for(int i = 0; i < tokens.size(); i++)
        {
            if(tokens.get(i).text.charAt(0) == '"' || ParserHelpers.IsNumber(tokens.get(i).text))
            {
                children.add(new ParameterBranch(tokens.get(i)));
            }else if(tokens.get(i).text.equals("("))
            {
                int end;
                end = ParserHelpers.FindMatching(
                    tokens,
                    i,
                    "(",
                    ")"
                );

                children.add(new FunctionBranch(tokens.subList(i-1, end)));
                i = end;
            }
        }

        return children;
    }
}
