package unanet.translator.ast;

import unanet.translator.exceptions.ParsingException;
import unanet.translator.exceptions.TokenException;
import unanet.translator.functions.LookupFuncs;
import unanet.translator.functions.Variables;
import java.util.List;

public class FileLoopBranch extends BaseBranch {
    BaseBranch fileId;
    BaseBranch searchColumn;
    BaseBranch searchStr;
    BaseBranch findColumn;
    BaseBranch varName;

    BlockBranch code;

    Token me;

    FileLoopBranch(List<Token> tokens) {
        me = tokens.get(0);

        int i = 1;
        try {
            int end = ParserHelpers.FindMatching(tokens, i, "(", ")");

            List<BaseBranch> params =
                ParserHelpers.parseParameters(tokens.subList(i+1, end));

            if(params.size() != 5) {
                throw new TokenException(this.me, String.format("Invalid parameter count. Expected 5, received %d.", params.size()));
            }else{
                fileId       = params.get(0);
                searchColumn = params.get(1);
                searchStr    = params.get(2);
                findColumn   = params.get(3);
                varName      = params.get(4);
            }

            //Skip after params, and skip closing paren.
            i = end+1;
        }catch(ParsingException ex) {
            ex.printStackTrace();
            throw new TokenException(this.me, "No matching parens to parameter list for ITERATEFILE.");
        }

        try {
            int end   = ParserHelpers.FindMatching(tokens, i, "{", "}");
            this.code = new BlockBranch(tokens.subList(i+1, end));
        }catch(ParsingException ex) {
            throw new TokenException(tokens.get(i), "No matching curly bracket for ITERATEFILE.");
        }
    }

    public String proc() {
        String file_id    = this.fileId.proc();
        String col_a      = this.searchColumn.proc();
        String col_b      = this.findColumn.proc();
        String search_for = this.searchStr.proc();
        String var_name   = this.varName.proc();

        List<String> matches =
            LookupFuncs.getAll(file_id, col_a, search_for, col_b);

        for(String match : matches) {
            Variables.SETVAR(var_name, match);
            String res = this.code.proc();
            if(res != null) {
                return res;
            }
        }

        return null;
    }
}
