package unanet.translator.ast;

public class ParameterBranch extends BaseBranch {

    private Token val;

    /**
     * A constructor that initializes the parameter to the value passed.
     * @param val The value that the parameter is equal to.
     */
    ParameterBranch(Token val) {
        if(ParserHelpers.IsLiteral(val.text))
        {
            val.text = ParserHelpers.TrimQuotes(val.text);
        }
        this.val = val;
    }

    /**
     * Processes this parameter branch.
     * @return Returns the value passed in the constructor.
     */
    @Override
    public String proc() {
        return val.text;
    }

    @Override
    public String toString() {
        if(ParserHelpers.IsNumber(val.text) && !val.text.isEmpty()) {
            return val.text;
        }else{
            return "\"" + val.text + "\"";
        }
    }
}
