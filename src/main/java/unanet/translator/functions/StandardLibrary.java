package unanet.translator.functions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import unanet.translator.Translator;
import unanet.translator.ast.ParserHelpers;
import unanet.translator.exceptions.FunctionResultException;

/**
 * @author David Bittner
 */

public class StandardLibrary {
    private static Logger logger = Logger.getGlobal();
    /**
     * Gets data from the input file.
     * @param num The column number you want to retrieve
     * @return The data found within that column
     */
    public static String C(final String num) {
        final String[] currentRow = Translator.GetCurrentRow();

        if(currentRow == null) {
            throw new FunctionResultException("There are no rows to choose from, are you in the header block?");
        }

        if(!ParserHelpers.IsNumber(num))
        {
            throw new FunctionResultException(String.format("'%s' is not a number.", num));
        }

        int index;
        try{
            index = Integer.parseUnsignedInt(num) - 1;
        }catch(final NumberFormatException ex) {
            throw new FunctionResultException(String.format("Index cannot be negative: '%s'", num));
        }

        if(index >= currentRow.length) {
            throw new FunctionResultException(
                    String.format("Invalid column number: '%s'. The number is too large, number of available columns is %d.",
                            num,
                            currentRow.length));
        }else if(index < 0) {
            throw new FunctionResultException(String.format("Invalid column passed: '%s'. The column number cannot be zero, or below zero!", num));
        }
        return currentRow[index];
    }

    /**
     * Makes a string uppercase.
     * @param str The string you want transformed
     * @return The uppercase string
     */
    public static String UPPER(final String str) {
        return str.toUpperCase();
    }
    /**
     * Makes a string lowercase.
     * @param str The string you want transformed
     * @return The lowercase string
     */
    public static String LOWER(final String str) {
        return str.toLowerCase();
    }

    /**
     * Concatenates a list of strings
     * @param strings The strings you want concatenated
     * @return The strings concatenated
     */
    public static String CONCAT(final String... strings)
    {
        final StringBuilder ret = new StringBuilder();
        for(final String str : strings)
        {
            ret.append(str);
        }
        return ret.toString();
    }

    /**
     * Outputs ""
     * @return ""
     */
    public static String BLANK()
    {
        return "";
    }
    
    /**
     * Used for fixed field formatted files
     * @param start The start index of the value
     * @param end The end index of the value
     * @return The trimmed row from 'start' to 'finish'
     */
    public static String FC(final String start, final String end)
    {
        if(!ParserHelpers.IsNumber(start) ||
           !ParserHelpers.IsNumber(end))
        {
            throw new FunctionResultException(String.format("'%s' is not a number.", end));
        }

        int start_int;
        int end_int;
        try{
            start_int = Integer.parseUnsignedInt(start);
            end_int   = Integer.parseUnsignedInt(end);
        }catch(final NumberFormatException ex) {
            throw new FunctionResultException(String.format("FC('%s', '%s') is not valid (maybe they are negative/aren't integers?).", start, end));
        }

        final StringBuilder totalList = new StringBuilder();
        final String[] row = Translator.GetCurrentRow();
        
        for(final String i : row)
        {
            totalList.append(i);
            if(totalList.length() > end_int)
            {
                break;
            }
        }
        if(totalList.length() <= end_int)
        {
            throw new FunctionResultException(
                    String.format("End value exceeds the length of row. You passed: '%s', while the row is '%d' characters long.",
                            end,
                            totalList.length()));
        }
        
        return totalList.toString().substring(start_int, end_int);
    }
    
    /**
     * Used to simply ignore a row in output. Discards all the output for this section.
     */
    public static void IGNORE()
    {
        Translator.IgnoreRow();
    }
    
    /**
     * Retrieves a certain amount of characters starting from the left of the string.
     * @param amount The amount of characters to trim
     * @param str The string to trim
     * @return The trimmed string
     */
    public static String LEFT(final String amount, final String str)
    {
        if(!ParserHelpers.IsNumber(amount))
        {
            throw new FunctionResultException(String.format("'%s' is not a number", amount));
        }
        
        final int amount_int = Integer.parseUnsignedInt(amount);
        return str.substring(0, amount_int);
    }
    
    /**
     * Retrieves a certain amount of characters starting from the right of the string.
     * @param amount The amount of strings to trim
     * @param str The string to trim
     * @return The trimmed string
     */
    public static String RIGHT(final String amount, final String str)
    {
        if(!ParserHelpers.IsNumber(amount))
        {
            throw new FunctionResultException(String.format("'%s' is not a number", amount));
        }
        
        final int amount_int = Integer.parseUnsignedInt(amount);
        return str.substring(str.length()-amount_int);
    }
    
    /**
     * A counter that increments with every row of output.
     * @param start The value to start the counter at
     * @return The counter's value
     */
    public static String SEQ(final String start)
    {
        if(!ParserHelpers.IsNumber(start))
        {
            throw new FunctionResultException(String.format("'%s' is not a number", start));
        }
        final int num = Integer.parseInt(start);
        return Integer.toString(num + Translator.GetCurrentRowNum());
    }
    
    /**
     * Returns the length of the passed string
     * @param str The string to check the length of
     * @return The length of the string
     */
    public static int LENGTH(final String str)
    {
        return str.length();
    }
    
    /**
     * Evaluates the given mathematical expression.
     * @param equation The equation to be evaluated
     * @param prec The level of precision to output
     * @return The value of the resulting expression
     */
    public static String MATH(final String equation, final String prec)
    {
        if(!ParserHelpers.IsNumber(prec))
        {
            throw new FunctionResultException(String.format("'%s' is not a number", prec));
        }


        final Expression ret = new ExpressionBuilder(equation)
                .build();

        return String.format("%."+prec+"f", ret.evaluate());
    }
    
    /**
     * Printing for logging purposes.
     * @param print_str The string that you want printed
     */
    public static void PRINT(final String print_str) {
        logger.info(print_str);
    }
    
    /**
     * Replaces a match in a string with something else, using a regex.
     * @param str The string to be acted upon
     * @param rep The regex to find matches
     * @param with What to replace matches with
     * @return The modified string
     */
    public static String REPLACE(final String str, final String rep, final String with)
    {
        return str.replaceAll(rep, with);
    }

    /**
     * Trims the whitespace from the edges of a string.
     * @param str The string to be trimmed.
     * @return The trimmed string.
     */
    public static String TRIM(final String str) {
        return str.trim();
    }

    /**
     * Trims the whitespace from the left side of a string.
     * @param str The string to be trimmed.
     * @return The trimmed string.
     */
    public static String LTRIM(final String str) {
        for(int i = 0; i < str.length(); i++) {
            if(!Character.isWhitespace(str.charAt(i))) {
                return str.substring(i, str.length());
            }
        }
        return "";
    }

    /**
     * Trims the whitespace from the right side of a string.
     * @param str The string to be trimmed.
     * @return The trimmed string.
     */
    public static String RTRIM(final String str) {
        for(int i = str.length()-1; i >= 0; i--) {
            if(!Character.isWhitespace(str.charAt(i))) {
                return str.substring(0, i+1);
            }
        }
        return "";
    }

    /**
     * Prints today's date in a given format.
     * @param date_str How to format today's date.
     * @return The date string from the given format.
     */
    public static String TODAY(final String date_str) {
        final Date curDat = new Date();
        final SimpleDateFormat form = new SimpleDateFormat(date_str);

        return form.format(curDat);
    }

    /**
     * A standard substring function, trims from the starting index to the end index.
     * @param str The string to trim
     * @param start Where to start trimming
     * @param end Where to stop strimming
     * @return The subsection of the string
     */
    public static String SUBSTR(final String str, final String start, final String end) {
        if(!ParserHelpers.IsNumber(start)) {
            throw new FunctionResultException(String.format("Start index must be a number, you gave '%s'", start));
        }

        if(!ParserHelpers.IsNumber(end)) {
            throw new FunctionResultException(String.format("Start index must be a number, you gave '%s'", end));
        }

        final int a = Integer.parseInt(start);
        final int b = Integer.parseInt(end);

        if(a < 0 || a > str.length()) {
            throw new FunctionResultException(String.format("Invalid start value ('%s') for string: '%s'", start, str));
        }else if(b < 0 || b > str.length()) {
            throw new FunctionResultException(String.format("Invalid end value ('%s') for string: '%s'", end, str));
        }else if(b < a) {
            throw new FunctionResultException(String.format("End index '%s' cannot be smaller than start index '%s'.", end, start));
        }

        return str.substring(a, b);
    }

    /**
     * Searches for a string occuring in another string and gives the index of where it occurs.
     * @param str The string to search through.
     * @param find The string to find within that string.
     * @return The index of the location it found the string in.
     */
    public static int SEARCH(final String str, final String find) {
        final int location = str.indexOf(find);
        return location;
    }

    /**
     * Checks whether or not the two values are equal
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not A and B are equal.
     */
    public static boolean EQUAL(final String a, final String b) {
        if(ParserHelpers.IsNumber(a) && ParserHelpers.IsNumber(b)) {
            final double a_num = Double.parseDouble(a);
            final double b_num = Double.parseDouble(b);

            return a_num == b_num;
        }else{
            return a.equals(b);
        }
    }

    /**
     * Checks whether or not the two values are not equal.
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not A and B are not equal
     */
    public static boolean NEQUAL(final String a, final String b) {
        return !a.equals(b);
    }

    /**
     * Checks whether or not a is greater than b
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not a is greater than b
     */
    public static boolean GREATER(final String a, final String b) {
        if(ParserHelpers.IsNumber(a) && ParserHelpers.IsNumber(b)) {
            final double a_num = Double.parseDouble(a);
            final double b_num = Double.parseDouble(b);

            return a_num > b_num;
        }else{
            return a.compareTo(b) > 0;
        }
    }

    /**
     * Checks whether or not a is less than b
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not a is less than b
     */
    public static boolean LESS(final String a, final String b) {
        if(ParserHelpers.IsNumber(a) && ParserHelpers.IsNumber(b)) {
            final double a_num = Double.parseDouble(a);
            final double b_num = Double.parseDouble(b);

            return a_num < b_num;
        }else{
            return a.compareTo(b) < 0;
        }
    }

    /**
     * Checks whether or not a is less than or equal to b
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not a is less than or equal to b
     */
    public static boolean LESSE(final String a, final String b) {
        if(ParserHelpers.IsNumber(a) && ParserHelpers.IsNumber(b)) {
            final double a_num = Double.parseDouble(a);
            final double b_num = Double.parseDouble(b);

            return a_num <= b_num;
        }else{
            return a.compareTo(b) <= 0;
        }
    }

    /**
     * Checks whether or not a is greater than or equal to b
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not a is greater than or equal to b
     */
    public static boolean GREATERE(final String a, final String b ){
        if(ParserHelpers.IsNumber(a) && ParserHelpers.IsNumber(b)) {
            final double a_num = Double.parseDouble(a);
            final double b_num = Double.parseDouble(b);

            return a_num >= b_num;
        }else{
            return a.compareTo(b) >= 0;
        }
    }

    /**
     * Checks whether or not both a and b are true
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not a and b are both true
     */
    public static boolean AND(final String a, final String b) {
        return a.equals("true") && b.equals("true");
    }

    /**
     * Checks whether or not a or b are true
     * @param a The first operand
     * @param b The second operand
     * @return Whether or not a or b are true
     */
    public static boolean OR(final String a, final String b) {
        return a.equals("true") || b.equals("true");
    }

    /**
     * Negates the given value (true becomes false)
     * @param a The operand
     * @return The opposite of a
     */
    public static boolean NOT(final String a) {
        if(a.equals("true")) {
            return false;
        }else if(a.equals("false")){
            return true;
        }else{
            throw new FunctionResultException(String.format("Attempted to invert '%s'", a));
        }
    }

    /**
     * Returns null, not sure when this would be used, but figured it might be helpful at some point.
     * @return Null
     */
    public static String NULL() {
        return null;
    }

    /**
     * Checks whether or not the given string is a number.
     * @param val The string to check whether or not it is a number.
     * @return Returns either 'true' or 'false'.
     */
    public static boolean ISNUMERIC(final String val) {
        return ParserHelpers.IsNumber(val);
    }

    /**
     * Specifies that a given block of code is only for logic.
     */
    public static void LOGICONLY() {
        Translator.LogicOnly();
    }

    /**
     * Sets the output headers.
     * @param headers The headers to use on the output file.
     */
    public static void HEADER(String headers) {
        headers = ParserHelpers.TrimQuotes(headers);
        Translator.SetOutputHeaders(headers.split(",", -1));
    }

    /**
     * Whether or not to output only unique rows. Must be sorted.
     * @param val Whether or not the enable UNIQUE (true or false)
     */
    public static void UNIQUE(final String val) {
        if(val.toLowerCase().equals("true")) {
            Translator.OnlyUniques();
        }
    }

    /**
     * Whether or not to sort the outputted rows.
     * @param val Whether or not the enable SORT (true or false)
     */
    public static void SORT(final String val) {
        if(val.toLowerCase().equals("true")) {
            Translator.SortOutput();
        }
    }

    /**
     * Tells the translator whether or not there are import headers.
     */
    public static void NOHEADERS() {
        Translator.IgnoreImportHeaders();
    }

    /**
     * Tells the translator whether or not there are import headers.
     * @param val Whether or not to enable NOHEADERS (true or false).
     */
    public static void NOHEADERS(final String val) {
        if(val.toLowerCase().equals("true")) {
            Translator.IgnoreImportHeaders();
        }
    }

    /**
     * Outputs 'true'
     * @return 'true'
     */
    public static String TRUE() {
        return "true";
    }

    /**
     * Outputs 'false'
     * @return 'false'
     */
    public static String FALSE() {
        return "false";
    }

    /**
     * Trims everything before the given string. AFTER("thisis@anemail.com", "@") will give "anemail.com".
     * @param str The string to trim
     * @param after What to search for
     * @return The trimmed string
     */
    public static String AFTER(final String str, final String after) {
        final int index = str.indexOf(after);
        if(index == -1) {
            throw new FunctionResultException(String.format("String '%s' does not exist in string '%s'.", after, str));
        }else{
            return str.substring(index+1);
        }
    }

    /**
     * Trims everything after the given string. BEFORE("thisis@anemail.com", "@") will give "thisis".
     * @param str The string to trim
     * @param before What substring to search for
     * @return The trimmed string
     */
    public static String BEFORE(final String str, final String before) {
        final int index = str.indexOf(before);
        if(index == -1) {
            throw new FunctionResultException(String.format("String '%s' does not exist in string '%s'.", before, str));
        }else{
            return LEFT(Integer.toString(index), str);
        }
    }

    /**
     * Returns true/false depending on whether or not a string contains another string.
     * @param str The string to search through
     * @param what What to search for in the string
     * @return Whether or not the parameter 'what' was found in the string
     */
    public static boolean HAS(final String str, final String what) {
        return str.contains(what);
    }

    /**
     * Returns a formatted date from an epoch string.
     * @param in_format The format of the inputted date.
     * @param out_format The format of the date to be returned.
     * @param time The date formatted in the format of in_format.
     * @return The input date formatted in the given format in out_format.
     */
    public static String DATE(final String in_format, final String out_format, final String time) {
        final SimpleDateFormat in_form = new SimpleDateFormat(in_format);

        try{
            final Date date = in_form.parse(time);

            return new SimpleDateFormat(out_format).format(date);
        }catch(final ParseException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse date '%s' in format '%s'.",
                    in_format,
                    time
                )
            );
        }
    }

    /**
     * Adds days, months, and years from a given date.
     * @param format The format of the given date.
     * @param in_date The first date to add from.
     * @param d The amount of days to add.
     * @param m The amount of months to add.
     * @param y The amount of years to add.
     * @return The formatted date with added months, days, or years.
     */
    public static String DATEADD(final String format, final String in_date, final String d, final String m, final String y) {
        final SimpleDateFormat in_form = new SimpleDateFormat(format);

        try{
            final Date date = in_form.parse(in_date);
            final int day = Integer.parseInt(d);
            final int month = Integer.parseInt(m);
            final int year = Integer.parseInt(y);

            final Calendar cal_date = new Calendar
                .Builder()
                .setInstant(date)
                .build(); 

            cal_date.add(Calendar.DAY_OF_MONTH, day);
            cal_date.add(Calendar.MONTH, month);
            cal_date.add(Calendar.YEAR, year);

            return in_form.format(cal_date.getTime());
        }catch(final ParseException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse date '%s' in format '%s'.",
                    format,
                    in_date
                )
            );
        }catch(final NumberFormatException ex) {
            throw new FunctionResultException(
                String.format(
                    "Unable to parse day: '%s', month: '%s', year: '%s'. One of these is not an integer.",
                    d, m, y
                )
            );
        }
    }

    /**
     * Subtracts days, months, and years from a given date.
     * @param format The format of the given date.
     * @param in_date The first date to subtract from.
     * @param d The amount of days to subtract.
     * @param m The amount of months to subtract.
     * @param y The amount of years to subtract.
     * @return The formatted date with subtracted months, days, or years.
     */
    public static String DATESUB(final String format, final String in_date, final String d, final String m, final String y) {
        return DATEADD(format, in_date, "-"+d, "-"+m, "-"+y);
    }

    /**
     * Compares two dates.
     * @param format The format of the given dates.
     * @param date_a The first date to be compared.
     * @param date_b The second date to be compared.
     * @return Returns either -1, 0, or 1. -1 if less than, 0 if equal, 1 if greater.
     */
    public static int DATECMP(final String format, final String date_a, final String date_b) {
        final SimpleDateFormat date_format = new SimpleDateFormat(format);
        
        Date a, b;
        try{
            a = date_format.parse(date_a);
        }catch(final ParseException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse date '%s' with the given format '%s'.",
                    format,
                    date_a
                )
            );
        }

        try{
            b = date_format.parse(date_b);
        }catch(final ParseException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse date '%s' with the given format '%s'.",
                    format,
                    date_b
                )
            );
        }

        if(a.before(b)) {
            return -1;
        }else if(a.equals(b)) {
            return 0;
        }else{
            return 1;
        }
    }

    /**
     * Returns the previous date in which a certain day of the week occurs. For example, give me the date of the previous sunday.
     * @param format The format of the given date.
     * @param date The current date (or whatever date you want).
     * @param day An integer from 0-6 that represents the day of the week to search for. 0 is Sunday.
     * @return The previous date in which the given day occurs formatted in the given format.
     */
    public static String DATEPREV(final String format, final String date, final String day) {
        final SimpleDateFormat date_format = new SimpleDateFormat(format);
        
        Date form_date;
        try{
            form_date = date_format.parse(date);
            final Calendar cal = new Calendar
                .Builder()
                .setInstant(form_date)
                .build(); 

            final int which_day = Integer.parseInt(day)+1;

            if(which_day > 7 || which_day < 1) {
                throw new FunctionResultException(
                    String.format(
                        "Expected number between 0-6, got '%d'.",
                        which_day
                    )
                );
            }

            if(cal.get(Calendar.DAY_OF_WEEK) < which_day) {
                cal.set(Calendar.WEEK_OF_MONTH, cal.get(Calendar.WEEK_OF_MONTH) - 1);
            }
            cal.set(Calendar.DAY_OF_WEEK, which_day);
            return date_format.format(cal.getTime());
        }catch(final ParseException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse date '%s' with the given format '%s'.",
                    format,
                    date
                )
            );
        }catch(final NumberFormatException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse given integer '%s'.",
                    day
                )
            );
        }
    }

    /**
     * Returns the next date in which a certain day of the week occurs. For example, give me the date of the next sunday.
     * @param format The format of the given date.
     * @param date The current date (or whatever date you want).
     * @param day An integer from 0-6 that represents the day of the week to search for. 0 is Sunday.
     * @return The next date in which the given day occurs formatted in the given format.
     */
    public static String DATENEXT(final String format, final String date, final String day) {
        final SimpleDateFormat date_format = new SimpleDateFormat(format);
        
        Date form_date;
        try{
            form_date = date_format.parse(date);
            final Calendar cal = new Calendar
                .Builder()
                .setInstant(form_date)
                .build(); 

            final int which_day = Integer.parseInt(day) + 1;

            if(which_day > 7 || which_day < 1) {
                throw new FunctionResultException(
                    String.format(
                        "Expected number between 0-6, got '%d'.",
                        which_day
                    )
                );
            }

            if(cal.get(Calendar.DAY_OF_WEEK) > which_day) {
                cal.set(Calendar.WEEK_OF_MONTH, cal.get(Calendar.WEEK_OF_MONTH) + 1);
            }
            cal.set(Calendar.DAY_OF_WEEK, which_day);
            return date_format.format(cal.getTime());
        }catch(final ParseException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse date '%s' with the given format '%s'.",
                    format,
                    date
                )
            );
        }catch(final NumberFormatException ex) {
            throw new FunctionResultException(
                String.format(
                    "Failed to parse given integer '%s'.",
                    day
                )
            );
        }
    }

    public static void QUIT(final String msg, final String code) {
        logger.severe(msg);
        System.exit(Integer.parseInt(code));
    }
}
