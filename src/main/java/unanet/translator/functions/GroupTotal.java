package unanet.translator.functions;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import unanet.translator.Translator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;

public class GroupTotal {
    static HashMap<String, Integer> totals;
    static ArrayList<Integer> columns;

    /**
     * Produces a count of how many times a specific ID occurs in a column.
     * @param id The ID to compare.
     * @return A temporary identifier to be replaced at the end of execution.
     */
    public static String GROUPTOTAL(String id) {
        if(totals == null) {
            totals = new HashMap<>();
            columns = new ArrayList<>();
        }

        String hash = id + Translator.GetCurrentColNum();

        if(totals.containsKey(hash)) {
            int prev = totals.get(hash) + 1;
            totals.put(hash, prev);
        }else{
            totals.put(hash, 1);
            if(columns.indexOf(Translator.GetCurrentColNum()) == -1) {
                columns.add(Translator.GetCurrentColNum());
            }
        }

        return hash;
    }

    /**
     * Tells whether or not GroupTotal was used. Used by the Translator class to determine whether or not the
     * temporary values need to be replaced.
     * @return A boolean that says whether or not a grouptotal function was called.
     */
    public static boolean WasCalled() {
        return totals != null;
    }

    /**
     * Iterates through and replaces the temporary placeholders with the actual count.
     * @param temp_reader The reader to read the placeholders in from.
     * @param writer The writer to write the replacements to.
     */
    public static void WriteTotals(Reader temp_reader, Writer writer) {
        BufferedReader reader = new BufferedReader(temp_reader);

        try {
            boolean[] logic_only_list = Translator.CheckLogicOnlyList();

            //Yay for logic only fields. This complicates things a lot.
            int relevant_cols[] = new int[columns.size()];
            int counter = 0;
            for(int i : columns) {
                int dec = 0;
                for(int j = 0; j < i; j++) {
                    if(logic_only_list[j]) {
                        dec++;
                    }
                }

                relevant_cols[counter] = i-dec;
                counter++;
            }

            CSVReader import_reader = new CSVReader(reader);
            CSVWriter export_writer = new CSVWriter(writer);

            String []cur_row;

            if(Translator.CheckHeaders() != null) {
                cur_row = import_reader.readNext();
                export_writer.writeNext(cur_row, false);
            }

            while((cur_row = import_reader.readNext()) != null) {

                for(int col : relevant_cols) {
                    String hash = cur_row[col];

                    cur_row[col] = Integer.toString(totals.get(hash));
                }
                export_writer.writeNext(cur_row, false);
            }
        }catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
