package unanet.translator.functions;

import java.util.HashMap;

public class Variables {
    static HashMap<String, String> vars;

    /**
     * Used to create a new variable (No longer required)
     * @param var The name of the variable
     */
    public static void NEWVAR(String var) {
        if(vars == null) {
            vars = new HashMap<>();
        }
    }

    /**
     * Used to set a variable to a certain value.
     * @param var The name of the variable to set or change.
     * @param val The value to set the variable to.
     */
    public static void SETVAR(String var, String val) {
        if(vars == null) {
            vars = new HashMap<>();
        }

        vars.put(var, val);
    }

    /**
     * Used to check of the value of a variable.
     * @param var The variable to check the value of.
     * @return The value of the variable.
     */
    public static String GETVAR(String var) {
        if(vars == null || !vars.containsKey(var)) {
            return "";
        }

        return vars.get(var);
    }
}
