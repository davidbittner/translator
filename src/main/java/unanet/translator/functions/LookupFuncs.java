package unanet.translator.functions;

import com.opencsv.CSVReader;
import unanet.translator.Translator;
import unanet.translator.exceptions.FunctionResultException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LookupFuncs {
    static class LookupFile {
        String[] headers;
        ArrayList<String[]> content;

        LookupFile(String[] headers) {
            content = new ArrayList<>();
            this.headers = headers;
        }
    }

    public static HashMap<String, LookupFile> files;

    /**
     * Checks whether or not a file has been loaded with the given ID already.
     * @param id The ID to check for.
     */
     static void checkExistence(String id) {
        if(files == null) {
            throw new FunctionResultException("No files are loaded.");
        }else if(!files.containsKey(id)) {
            StringBuilder formatter = new StringBuilder(String.format("File by the ID '%s' is not loaded. Candidates are:\n", id));

            files.forEach((a, b) -> {
                formatter.append(String.format("\t%s\n", a));
            });

            throw new FunctionResultException(formatter.toString());
        }
    }

    /**
     * Retrieves the given column from a file based on the header name.
     * @param id The ID of the file to check.
     * @param name The header of the column being searched for.
     * @return The column index.
     */
    private static int getColumn(String id, String name) {
        LookupFile lookup = files.get(id);
        for(int i = 0; i < lookup.headers.length; i++) {
            if(lookup.headers[i].equals(name)) {
                return i;
            }
        }

        throw new FunctionResultException(String.format("Column '%s' does not exist in file '%s'", name, id));
    }

    /**
     * Loads a file and assigns it a given ID.
     * @param filename The name of the file to load.
     * @param file_id The ID to be assigned to the file.
     */
    private static void loadFile(String filename, String file_id) {
        if(files == null) {
            files = new HashMap<>();
        }else if(files.containsKey(file_id)) {
            throw new FunctionResultException(String.format("File with the id '%s' already loaded.", file_id));
        }

        try{
            Reader read = new FileReader(new File(filename));
            CSVReader reader = new CSVReader(read);

            String[] row = reader.readNext();
            LookupFile add = new LookupFile(row);

            while((row = reader.readNext()) != null) {
                add.content.add(row);
            }

            files.put(file_id, add);
            reader.close();
        }catch(FileNotFoundException ex) {
            throw new FunctionResultException(String.format("Attempted to load non-existent file: '%s'", filename));
        }catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * The public access point for a user through a template file for loading a file.
     * @param filename The name of the file itself to be loaded.
     * @param file_id The ID to be assigned to the file.
     */
    public static void LOAD(String filename, String file_id) {
        loadFile(filename, file_id);
    }

    /**
     * Loads a file and makes sure it has at least min_lines number of lines.
     * @param filename The name of the file to load.
     * @param file_id The ID to give to the file after it is loaded.
     * @param min_lines The minimum number of lines the file must have.
     */
    public static void LOADMIN(String filename, String file_id, String min_lines) {
        loadFile(filename, file_id);

        LookupFile loaded_file = files.get(file_id);

        long lines = loaded_file.content.size();
        if(lines < Long.parseLong(min_lines)) {
            files.put(file_id, null);
            throw new FunctionResultException(String.format("File '%s' does not have enough lines (has '%d', needs at least '%s').", filename, lines, min_lines));
        }
    }

    /**
     * Loads a file and makes sure it has enough lines relative to the length of the import file.
     * @param filename The name of the file to import.
     * @param file_id The ID to give to the file after it is loaded.
     * @param relative_min_lines How many lines relative to the main import file this file must have.
     */
    public static void LOADRELATIVEMIN(String filename, String file_id, String relative_min_lines) {
        loadFile(filename, file_id);
        long min_lines = Long.parseLong(relative_min_lines);

        LookupFile loaded_file = files.get(file_id);

        long lines = loaded_file.content.size();
        if(lines < Translator.GetImportLineCount() - min_lines) {
            files.put(file_id, null);
            throw new FunctionResultException(String.format("File '%s' does not have enough lines (has '%d', needs at least '%s').", filename, lines, Translator.GetImportLineCount() - min_lines));
        }
    }

    /**
     * Checks whether or not an entry exists within a loaded file.
     * @param file_id The ID of the file to search through.
     * @param column_header The name of the header of the column to search through.
     * @param entry What entry to search for.
     * @return A boolean that states whether or not the entry exists in that column.
     */
    public static boolean EXISTS(String file_id, String column_header, String entry) {
        checkExistence(file_id);

        int column_index = getColumn(file_id, column_header);
        LookupFile lookup = files.get(file_id);
        for(String[] row : lookup.content) {
            if(row[column_index].equals(entry)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Looks up a matching entry in a loaded file.
     * @param file_id The ID of the file to search through.
     * @param col_a The name of the header of the first column.
     * @param look_for What to look for in the first column.
     * @param col_b The column to retrieve the matching data.
     * @return The matching value found in the 'B' column.
     */
    public static String LOOKUP(String file_id, String col_a, String look_for, String col_b) {
        checkExistence(file_id);

        LookupFile lookup = files.get(file_id);
        int col_a_index = getColumn(file_id, col_a);
        int col_b_index = getColumn(file_id, col_b);

        for(String[] row : lookup.content) {
            if(row[col_a_index].equals(look_for)) {
                return row[col_b_index];
            }
        }

        throw new FunctionResultException(String.format("Could not find entry '%s' under header '%s' in file '%s'.", look_for, col_a, file_id));
    }

    /**
     * Counts how many matching entries there are in a loaded file.
     * @param file_id The ID of the file to search through.
     * @param col_a The column to search through.
     * @param look_for What to look for.
     * @return The number of occurrences of $look_for in $col_a.
     */
    public static int COUNT(String file_id, String col_a, String look_for) {
        checkExistence(file_id);
        LookupFile lookup = files.get(file_id);
        int col_a_index = getColumn(file_id, col_a);

        int ret = 0;
        for(String []row: lookup.content) {
            if(row[col_a_index].equals(look_for)) {
                ++ret;
            }
        }

        return ret;
    }

    public static List<String> getAll(String file_id, String col_a, String look_for, String col_b) {
        checkExistence(file_id);
        List<String> ret = new ArrayList<>();

        LookupFile lookup = files.get(file_id);
        int col_a_index = getColumn(file_id, col_a);
        int col_b_index = getColumn(file_id, col_b);

        for(String[] row : lookup.content) {
            if(row[col_a_index].equals(look_for)) {
                ret.add(row[col_b_index]);
            }
        }

        return ret;
    }
}
