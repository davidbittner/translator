package unanet.translator;

import unanet.translator.ast.BlockBranch;
import unanet.translator.ast.ParserHelpers;
import unanet.translator.ast.Token;
import unanet.translator.exceptions.ParsingException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * The storage container for a chunk of code
 */
public class Code
{
    /**
     * The list
     */
    static final String SPLITTERS[] = { "<=", ">=", "!=", "(", ")", "{", "}", ",", "<", "=", ">"," "};

    String code;

    private BlockBranch root;
    
    public Code() {
        code = "";
    }

    /**
     * Adds a line of code to a code block.
     * @param line The line to be added
     */
    public void AddLine(final String line) {
        code = code + "\n" + line;
    }

    private int CountString(String str, String find_str) {
        int count = 0;
        int loc;
        while((loc = str.indexOf(find_str)) != -1) {
            count++;
            str = str.substring(loc+1);
        }

        return count;
    }

    /*
     * In previous versions of the CSV translator, comparison operators
     * were allowed. To maintain backwards compatibility, a function
     * was written to convert any operator comparisons to function calls.
     */
    void SubForBackwardsCompat(ArrayList<Token> tokens) {
        HashMap<String, String> subs = new HashMap<>();
        subs.put("<=", "LESSE");
        subs.put(">=", "GREATERE");
        subs.put("!=", "NEQUAL");
        subs.put("=",  "EQUAL");
        subs.put("<",  "LESS");
        subs.put(">",  "GREATER");

        for(int i = 0; i < tokens.size(); i++) {
            if(subs.containsKey(tokens.get(i).text)) {
                if(ParserHelpers.IsNumber(tokens.get(i-1).text) ||
                   ParserHelpers.IsLiteral(tokens.get(i-1).text)) {
                    tokens.add(i-1, new Token(subs.get(tokens.get(i).text), -1, -1));
                    i++;
                    tokens.add(i-1, new Token("(", -1, -1));
                    i++;
                    tokens.set(i, new Token(",", -1, -1));
                }else{
                    ArrayList<Token> temp_trimmed = new ArrayList<>(tokens.subList(0, i));
                    Collections.reverse(temp_trimmed);

                    int from_here = ParserHelpers.FindMatching(temp_trimmed, 0, ")", "(");
                    tokens.add(i-from_here-2, new Token(subs.get(tokens.get(i).text), -1, -1));
                    tokens.add(i-from_here-1, new Token("(", -1, -1));

                    i+=2;

                    tokens.set(i, new Token(",", -1, -1));
                }

                if(ParserHelpers.IsNumber(tokens.get(i+1).text) ||
                   ParserHelpers.IsLiteral(tokens.get(i+1).text)) {
                    tokens.add(i+2, new Token(")", -1, -1));
                }else{
                    i+=2;
                    int end = ParserHelpers.FindMatching(tokens, i, "(", ")");
                    tokens.add(end+1, new Token(")", -1, -1));
                }
            }
        }
    }

    /**
     * Tokenizes a chunk of code
     * @param relative_start The relative start of the line to put into the tokens for error messages.
     * @return The set of tokens stored as an ArrayList
     */
     public ArrayList<Token> Tokenize(int relative_start) {
        String chunk = code.trim();
        
        ArrayList<String> output = new ArrayList<>();

        //Split on quotes first
        for(int i = 0; i < chunk.length(); i++)
        {
            if(chunk.charAt(i) == '"') {
                int next_quote = i+1;
                while(chunk.charAt(next_quote) != '"')
                {
                    next_quote++;
                    if(next_quote >= chunk.length())
                    {
                        throw new ParsingException(String.format("ERROR: No matching quote found:\n\t%s", chunk));
                    }
                }

                String quotedBlock = chunk.substring(i, next_quote+1); 
                output.add(chunk.substring(0, i));
                output.add(quotedBlock);

                chunk = chunk.substring(next_quote+1, chunk.length());
                i = 0;
            }else if(chunk.charAt(i) == '#')
            {
                int new_line = chunk.substring(i).indexOf('\n');
                if(new_line != -1 ){
                    new_line += i;
                    output.add(chunk.substring(0, i));

                    chunk = chunk.substring(new_line);

                    i = 0;
                }else{
                    output.add(chunk.substring(0, i));

                    chunk = "";
                    i = 0;
                }
            }
        }
        output.add(chunk);
        
        //Replace adjacent whitespace
        for(int i = 0; i < output.size(); i++)
        {
            if(output.get(i).isEmpty()) {
                output.remove(i);
                i--;
                continue;
            }
            //Not a literal
            if(output.get(i).charAt(0) != '"')
            {
                output.set(i, output.get(i).replaceAll("( |\t|\n)+", " "));
            }
            //Trim whitespace
            output.set(i, output.get(i).trim());
        }

        int start_code = 0;
        while(start_code != output.hashCode()) {
            start_code = output.hashCode();
            ArrayList<String> before = new ArrayList<>(output);
            output.clear();

            for(String sub_chunk : before) {
                if(ParserHelpers.IsLiteral(sub_chunk)) {
                    output.add(sub_chunk);
                    continue;
                }
                for(String split : SPLITTERS) {
                    int loc = sub_chunk.indexOf(split);
                    if(loc != -1) {
                        output.add(sub_chunk.substring(0, loc));
                        output.add(sub_chunk.substring(loc, split.length() + loc));

                        sub_chunk = sub_chunk.substring(split.length() + loc);
                    }
                }
                if(!sub_chunk.isEmpty()) {
                    output.add(sub_chunk);
                }
            }
        }

        for(int i = 0; i < output.size(); i++) {
            if(output.get(i).trim().isEmpty()) {
                output.remove(i);
                i--;
            }
        }

        ArrayList<Token> tokens = new ArrayList<>();
        StringBuilder source_copy = new StringBuilder(code);

        for(String tok_text : output) {
            int token_loc = source_copy.indexOf(tok_text);
            int trimmed = code.length() - source_copy.length();
            source_copy = new StringBuilder(source_copy.substring((token_loc + tok_text.length())));

            int last_newline = code.substring(0, (token_loc + trimmed)).lastIndexOf("\n");
            int line_num     = CountString(code.substring(0, trimmed), "\n")+1;

            tokens.add(new Token(tok_text, line_num + relative_start, (trimmed+token_loc) - last_newline));
        }
        return tokens;
    }

    /**
     * Generates the syntax tree for this block
     * @param relative_start The relative start of the line so it can point to the right line in error messages.
     * @return Whether it succeeded or not
     * @throws ParsingException Throws a parsing exception in case some of the parsing down the line failed.
     */
    public boolean Lex(int relative_start) throws ParsingException
    {
        ArrayList<Token> tokens = Tokenize(relative_start);
        SubForBackwardsCompat(tokens);

        root = new BlockBranch(tokens);

        return true;
    }
    
    /**
     * Used to execute a block of code
     * @return The output of a block of code to be added to the output
     */
    public String Execute() {
        return root.proc();
    }
    
    public boolean isEmpty()
    {
        return code.isEmpty();
    }

    @Override
    public String toString() {
        if(root == null) {
            return "";
        }else{
            return root.toString();
        }
    }
}
