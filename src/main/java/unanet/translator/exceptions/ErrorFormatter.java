package unanet.translator.exceptions;

import unanet.translator.Translator;
import unanet.translator.ast.Token;

public class ErrorFormatter {
    static public String FormatError(Token tok, String msg) {
        StringBuilder formatter = new StringBuilder();

        formatter.append(String.format("ERROR: line %d character %d\n", tok.line, tok.character));
        formatter.append(String.format("This error occurred while at row: '%d', column: '%d'\n", Translator.GetCurrentRowNum(), Translator.GetCurrentColNum()));
        formatter.append(String.format("Message:\n\t%s\n", msg.replaceAll("\\n", "\n\t")));

        return formatter.toString();
    }
}
