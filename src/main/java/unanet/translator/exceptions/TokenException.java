package unanet.translator.exceptions;

import unanet.translator.ast.Token;

public class TokenException extends RuntimeException {

    private static final long serialVersionUID = 2288482351848361879L;

    public Token failedAt;

    public TokenException(Token failedAt, String message)
    {
        super(message);
        this.failedAt = failedAt;
    }

}
