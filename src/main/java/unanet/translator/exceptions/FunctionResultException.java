package unanet.translator.exceptions;

public class FunctionResultException extends RuntimeException {
    private static final long serialVersionUID = 2288482351848361879L;

    public FunctionResultException(String message)
    {
        super(message);
    }
}
