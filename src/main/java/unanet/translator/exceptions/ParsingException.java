package unanet.translator.exceptions;

public class ParsingException extends RuntimeException {

    private static final long serialVersionUID = 2288482351848361879L;

    public ParsingException(String message)
    {
        super(message);
    }

}
