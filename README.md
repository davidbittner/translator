[![pipeline status](https://gitlab.com/davidbittner/translator/badges/master/pipeline.svg)](https://gitlab.com/davidbittner/translator/commits/master)
[![coverage report](https://gitlab.com/davidbittner/translator/badges/master/coverage.svg)](https://gitlab.com/davidbittner/translator/commits/master)

# Translator

This is a program used for converting CSV data to match the expected imports of a system. It uses a simple syntax that can be found in detail at: [info](http://davidbittner.gitlab.io/translator/)

# Command Line Arguments

-import [file]
Import is used to specify the file that the program will be reading from.

-export [file]
Export is used to point to the location that the program will write to

-template [file]
Template is the file that the program should read for the intermediate script.
